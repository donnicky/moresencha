function startFiddleApplication() {
	Ext.define('MyGrid', {
		extend: 'Ext.grid.Panel',
		alias: 'widget.mygrid',
		plugins: {
			ptype: 'cellediting',
			clicksToEdit: 1,
			listeners: {
				beforeedit: function (editor, context) {
					console.log('beforeedit: ' + context.column.text);
				},
				edit: function (editor, context) {
					console.log('edit: ' + context.column.text);
				}
			}
		},
		store: {
			fields: ['id', 'name'],
			data: [{
				id: 1,
				name: 'rec1'
			}, {
				id: 2,
				name: 'rec2'
			}]
		},
		columns: [{
			text: 'id',
			dataIndex: 'id',
			flex: 1,
			editor: {
				type: 'numberfield'
			}
		}, {
			text: 'name',
			dataIndex: 'name',
			flex: 1,
			editor: {
				type: 'textfield'
			}
		}]
	});

	Ext.widget('mygrid', {
		renderTo: Ext.getBody()
	});
}