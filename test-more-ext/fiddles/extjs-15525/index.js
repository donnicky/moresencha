Ext.application({
	name: 'Fiddle',

	launch: function () {
		Ext.define('RootElement', {
			extend: 'Ext.data.TreeModel',
			childType: 'ChildElement',
			proxy: {
				type: 'ajax',
				api: {
					create: 'fiddles/extjs-15525/create.json',
					read: 'fiddles/extjs-15525/data.json',
					update: 'http://aaaaaa/UpdateRootElement'
				}
			}
		});

		Ext.define('ChildElement', {
			extend: 'Ext.data.TreeModel',
			proxy: {
				type: 'ajax',
				api: {
					update: 'http://aaaaaa/UpdateChildElement'
				}
			}
		});

		Ext.define('MyStore', {
			extend: 'Ext.data.TreeStore',
			model: 'RootElement',
			autoSync: true
		});

		var store = Ext.create('MyStore');

		Ext.widget('treepanel', {
			renderTo: Ext.getBody(),
			store: store,
			rootVisible: false,
			tbar: [{
				xtype: 'button',
				text: 'Add record',
				handler: function (btn) {
					var newRec = Ext.create('RootElement');
					store.getRoot().insertChild(0, newRec);
				}
			}],
			columns: [{
				xtype: 'treecolumn',
				text: 'text',
				dataIndex: 'text',
				flex: 1
			}, {
				text: 'descr',
				dataIndex: 'descr',
				flex: 1

			}, {
				text: 'class',
				renderer: function (value, meta, record) {
					return Ext.getClassName(record);
				}
			}],
			listeners: {
				select: function (cmp, record) {
					console.log(record.get('text'));
				}
			}
		});
	}
});