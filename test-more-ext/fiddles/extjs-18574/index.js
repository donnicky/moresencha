function startFiddleApplication() {
	var states = Ext.create('Ext.data.Store', {
		fields: ['abbr', 'name'],
		data: [
			{"abbr": "AL", "name": "Alabama"},
			{"abbr": "AK", "name": "Alaska"},
			{"abbr": "AZ", "name": "Arizona"}
		]
	});
	Ext.create('Ext.form.ComboBox', {
		fieldLabel: 'Choose State',
		pageSize: 2,
		store: states,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'abbr',
		renderTo: Ext.getBody()
	});

	// Attempting to "drop" this combo AFTER dropping the first one will fail due to:
	// Ext.ComponentManager.register(): Registering duplicate component id "undefined-paging-toolbar"
	Ext.create('Ext.form.ComboBox', {
		fieldLabel: 'Drop me Im Broken',
		pageSize: 2,
		store: states,
		queryMode: 'local',
		displayField: 'name',
		valueField: 'abbr',
		renderTo: Ext.getBody()
	});
}