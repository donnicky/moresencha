startTest(function (t) {

	function initStateProvider(next) {
		var stateProvider = Ext.create('Mext.state.DirectProvider', {
			directGetStateFn: DirectApi.getState,
			directSetStateFn: DirectApi.setState,
			directClearStateFn: DirectApi.clearState
		});

		var gettingState = t.beginAsync(30000, function () {
			t.fail('Mext.state.DirectProvider.getState() call timeout.');
			return true;
		});

		stateProvider.getState(function (err) {
			t.endAsync(gettingState);
			console.log(stateProvider.state);
			t.ok(!err, 'State successfully got from direct service.');
			if (!err) {
				Ext.state.Manager.setProvider(stateProvider);
				next();
			}
		});
	}

	var stateId = new Date().getTime().toString();
	console.log('stateId = ' + stateId);

	function createPanel() {
		var panel = Ext.widget('panel', {
			renderTo: Ext.getBody(),
			title: 'Panel',
			collapsible: true,
			titleCollapse: true,
			floatable: false,
			stateful: true,
			stateId: stateId,
			width: 200,
			height: 200
		});

		return panel;
	}

	t.diag('Test required Direct API methods');
	var loadingRemoteApi = t.beginAsync(30000, function () {
		t.fail('Direct API loading timeout.');
		return true;
	});

	t.chain(
		function (next) {
			Mext.direct.Manager.initDefaultRemoteApi().then(function () {
				t.pass('Direct API loaded.');
				t.endAsync(loadingRemoteApi);
				next();
			}).catch(function () {
				t.endAsync(loadingRemoteApi);
				t.fail('Direct API loading failed.')
			});
		},
		function (next) {
			var settingState = t.beginAsync(30000, function () {
				t.fail('DirectApi.setState() call timeout.');
				return true;
			});

			DirectApi.setState({key: 'testKey1', value: 'testValue1'}, function (data, response, success) {
				t.endAsync(settingState);
				t.ok(success, 'DirectApi.setState() completed successfully.');
				next();
			});
		},
		function (next) {
			var gettingState = t.beginAsync(30000, function () {
				t.fail('DirectApi.getState() call timeout.');
				return true;
			});

			DirectApi.getState(function (data, response, success) {
				t.endAsync(gettingState);
				t.ok(success, 'DirectApi.getState() completed successfully.');
				t.ok(Ext.isObject(data), 'Returned value is plain object.');
				t.is(data.testKey1, 'testValue1', 'State has previously set value.');
				next();
			});
		},
		function (next) {
			var settingState = t.beginAsync(30000, function () {
				t.fail('Another DirectApi.setState() call timeout.');
				return true;
			});

			DirectApi.setState({key: 'testKey2', value: 'testValue2'}, function (data, response, success) {
				t.endAsync(settingState);
				t.ok(success, 'DirectApi.setState() completed successfully.');
				next();
			});
		},
		function (next) {
			var gettingState = t.beginAsync(30000, function () {
				t.fail('Another DirectApi.getState() call timeout.');
				return true;
			});

			DirectApi.getState(function (data, response, success) {
				t.endAsync(gettingState);
				t.ok(success, 'DirectApi.getState() completed successfully.');
				t.ok(Ext.isObject(data), 'Returned value is plain object.');
				t.is(data.testKey1, 'testValue1', 'State has 1st value.');
				t.is(data.testKey2, 'testValue2', 'State has 2nd value.');
				next();
			});
		},
		function (next) {
			var clearingState = t.beginAsync(30000, function () {
				t.fail('Another DirectApi.getState() call timeout.');
				return true;
			});

			DirectApi.clearState('testKey1', function (data, response, success) {
				t.endAsync(clearingState);
				t.ok(success, 'DirectApi.clearState() completed successfully.');
				next();
			});
		},
		function (next) {
			var gettingState = t.beginAsync(30000, function () {
				t.fail('Another DirectApi.getState() call timeout.');
				return true;
			});

			DirectApi.getState(function (data, response, success) {
				t.endAsync(gettingState);
				t.ok(success, 'DirectApi.getState() completed successfully.');
				t.ok(data.testKey1 === void 0, 'Key "testKey1" is not defined in state.');
				t.is(data.testKey2, 'testValue2', 'State has 2nd value after deletion of the 1st.');
				next();
			});
		},
		function (next) {
			t.diag('Test DirectProvider');
			initStateProvider(next);
		},
		function (next) {
			var panel = createPanel();
			t.ok(!!panel.el.dom, 'Panel created.');
			//t.click(panel.down('title'), function () {
			//	t.ok(panel.getCollapsed(), 'Panel is collapsed after click.');
			//	next(panel);
			//});
			panel.on('collapse', function () {
				t.ok(panel.getCollapsed(), 'Panel is collapsed after click.');
				next(panel);
			}, panel, {single: true});
			t.click(panel.down('title'));
		},
		function (next, panel) {
			var panelId = panel.el.dom.id;
			panel.destroy();
			t.notOk(!!document.getElementById(panelId), 'Panel destroyed.');
			next();
		},
		function (next) {
			initStateProvider(next);
		},
		function (next) {
			var panel = createPanel();
			t.ok(panel.getCollapsed(), 'Panel is collapsed after recreation.');
			next();
		}
	);
});

