startTest(function (t) {
	var loadingRemoteApi = t.beginAsync(30000, function () {
		t.fail('Direct API loading timeout.');
		return true;
	});

	Mext.direct.Manager.initRemoteApi('/missingrpc', 'Ext.app.REMOTING_API').then(function () {
		t.endAsync(loadingRemoteApi);
		t.fail('Direct API manager did not report error correctly.');
	}).catch(function (err) {
		t.endAsync(loadingRemoteApi);
		t.ok(err, 'Direct API manager reported error correctly.');
	});
});