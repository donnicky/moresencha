startTest(function (t) {
	t.diag('Loading Direct API');
	var loadingRemoteApi = t.beginAsync(3000, function () {
		t.fail('Direct API loading timeout.');
		return true;
	});

	t.chain(
		function (next) {
			Mext.direct.Manager.initDefaultRemoteApi().then(function () {
				t.pass('Direct API loaded.');
				t.endAsync(loadingRemoteApi);
				next();
			}).catch(function () {
				t.endAsync(loadingRemoteApi);
				t.fail('Direct API loading failed.')
			});
		},
		function (next) {
			t.diag('Call the same method twice within one request');
			callTwoMethodsWithinOnceRequest('longRunningMethod1', 'longRunningMethod1', 0, 1, true, next);
		},
		function (next) {
			t.diag('Call the 2 different methods within one request');
			callTwoMethodsWithinOnceRequest('longRunningMethod1', 'longRunningMethod2', 0, 1, true, next);
		},
		function (next) {
			t.diag('Call the 2 different methods with 2 requests');
			callTwoMethodsWithinOnceRequest('longRunningMethod1', 'longRunningMethod2', 300, 2, true, next);
		},
		function (next) {
			t.diag('Call the same ASYNC method twice within one request');
			callTwoMethodsWithinOnceRequest('longRunningAsyncMethod1', 'longRunningAsyncMethod1', 0, 1, false, next);
		},
		function (next) {
			t.diag('Call the same ASYNC method twice within 2 requests');
			callTwoMethodsWithinOnceRequest('longRunningAsyncMethod1', 'longRunningAsyncMethod1', 300, 2, true, next);
		},
		function (next) {
			next();
		}
	);

	function callTwoMethodsWithinOnceRequest(method1, method2, callsTimeout, maxNumberOfRequests, isSequentialRun, next) {
		var methodsCompleted = 0;
		var numberOfRequests = 0;

		function onAjaxBeforeRequest() {
			numberOfRequests++;
		}

		Ext.Ajax.on('beforerequest', onAjaxBeforeRequest);
		var startTime = new Date().getTime();
		DirectApi[method1](function () {
			methodsCompleted++;
		});

		var callMethod2 = function () {
			DirectApi[method2](function () {
				methodsCompleted++;
			});
		};

		if (callsTimeout)
			setTimeout(callMethod2, callsTimeout);
		else
			callMethod2();

		t.waitForFn(function () {
			return methodsCompleted == 2;
		}, function () {
			var endTime = new Date().getTime();
			t.ok(numberOfRequests === maxNumberOfRequests, 'Two methods are sent within defined number of requests: ' + maxNumberOfRequests);
			if (isSequentialRun)
				t.ok(endTime - startTime > 6000, 'Methods are run SEQUENTIALLY server-side.');
			else
				t.ok(endTime - startTime < 5000, 'Methods are run CONCURRENTLY server-side.');
			Ext.Ajax.un('beforerequest', onAjaxBeforeRequest);
			next();
		});
	}
});
