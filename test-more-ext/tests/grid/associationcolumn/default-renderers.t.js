startTest(function (t) {

	t.chain(
		function (next) {
			prepare(next);
		},
		function (next, common) {

			var ordersStore = common.ordersStore,
				orderTypesStore = common.orderTypesStore;

			Ext.widget('grid', {
				renderTo: Ext.getBody(),
				store: ordersStore,
				columns: [
					{
						dataIndex: 'accountId',
						xtype: 'mext-associationcolumn',
						emptyValueText: '-',
						displayField: 'name'
					},
					{
						dataIndex: 'contactId',
						xtype: 'mext-associationcolumn',
						emptyValueText: '-',
						displayField: 'name'
					},
					{
						dataIndex: 'typeId',
						xtype: 'mext-associationcolumn',
						emptyValueText: '-',
						store: orderTypesStore,
						displayField: 'name'
					}
				]
			});

			ordersStore.load({
				callback: function (records, operation, success) {
					if (success)
						next();
					else
						t.fail('Cannot load orders.');
				}
			});
		},
		function (next) {
			t.is(t.getCell('grid', 0, 1).child('*').getHtml(), 'loading...', 'Cell loading state correctly shown.');
			t.is(t.getCell('grid', 3, 0).child('*').getHtml(), '-', 'Empty value correctly shown.');
			next();
		},
		{waitFor: 1000},
		function (next) {
			t.is(t.getCell('grid', 0, 0).child('*').getHtml(), 'Acc1', 'Nested data correctly shown.');
			t.is(t.getCell('grid', 0, 1).child('*').getHtml(), 'Ctc1', 'Loaded data correctly shown.');
			t.is(t.getCell('grid', 0, 2).child('*').getHtml(), 'Purchase', 'Data from store correctly shown.');
			t.is(t.getCell('grid', 2, 2).child('*').getHtml(), 'N/A', 'Data with ID=0 from store correctly shown.');
			t.is(t.getCell('grid', 1, 1).child('*').getHtml(), "<span style=\"color:magenta\">not found!</span>", 'Not found case correctly shown.');
			t.is(t.getCell('grid', 2, 1).child('*').getHtml(), "<span style=\"color:red\">error!</span>", 'Cell loading error correctly shown.');
			next();
		},
		function () {
		}
	);

});
