startTest(function (t) {

	t.chain(
		function (next) {
			prepare(next);
		},
		function (next, common) {

			var ordersStore = common.ordersStore,
				orderTypesStore = common.orderTypesStore;

			Ext.widget('grid', {
				renderTo: Ext.getBody(),
				store: ordersStore,
				columns: [
					{
						dataIndex: 'accountId',
						xtype: 'mext-associationcolumn',
						displayField: 'name',
						emptyValueText: '-',
						valueRenderer: function (valueRecord) {
							return valueRecord.getId() + valueRecord.get('name');
						}
					},
					{
						dataIndex: 'contactId',
						xtype: 'mext-associationcolumn',
						displayField: 'name',
						emptyValueText: '-',
						valueRenderer: function (valueRecord) {
							return valueRecord.getId() + valueRecord.get('name');
						},
						errorRenderer: function (value) {
							return '<b>' + value + '</b>';
						},
						notFoundRenderer: function () {
							return 'NOTFOUND';
						}
					},
					{
						dataIndex: 'typeId',
						xtype: 'mext-associationcolumn',
						store: orderTypesStore,
						displayField: 'name',
						emptyValueText: '-',
						valueRenderer: function (valueRecord) {
							return valueRecord.getId() + valueRecord.get('name');
						}
					}
				]
			});

			ordersStore.load({
				callback: function (records, operation, success) {
					if (success)
						next();
					else
						t.fail('Cannot load orders.');
				}
			});
		},
		function (next) {
			t.is(t.getCell('grid', 0, 1).child('*').getHtml(), 'loading...', 'Cell loading state correctly shown.');
			t.is(t.getCell('grid', 3, 0).child('*').getHtml(), '-', 'Empty value correctly shown.');
			next();
		},
		{waitFor: 1000},
		function (next) {
			t.is(t.getCell('grid', 0, 0).child('*').getHtml(), '1Acc1', 'Nested data correctly shown.');
			t.is(t.getCell('grid', 0, 1).child('*').getHtml(), '1Ctc1', 'Loaded data correctly shown.');
			t.is(t.getCell('grid', 0, 2).child('*').getHtml(), '1Purchase', 'Data from store correctly shown.');
			t.is(t.getCell('grid', 1, 1).child('*').getHtml(), "NOTFOUND", 'Not found case correctly shown.');
			t.is(t.getCell('grid', 2, 1).child('*').getHtml(), "<b>error!</b>", 'Cell loading error correctly shown.');
			next();
		},
		function () {
		}
	);

});
