function prepare(cb) {
	Mext.direct.Manager.initDefaultRemoteApi().then(function () {
		Ext.define('TestMext.OrderType', {
			extend: 'Ext.data.Model'
		});

		Ext.define('TestMext.Account', {
			extend: 'Ext.data.Model',
			proxy: {
				type: 'direct',
				api: {
					read: 'DirectApi.getAccounts'
				}
			}
		});

		Ext.define('TestMext.Contact', {
			extend: 'Ext.data.Model',
			proxy: {
				type: 'direct',
				api: {
					read: 'DirectApi.getContacts'
				}
			}
		});

		Ext.define('TestMext.Order', {
			extend: 'Ext.data.Model',
			fields: [
				{
					name: 'accountId',
					reference: 'TestMext.Account'
				},
				{
					name: 'contactId',
					reference: 'TestMext.Contact'
				},
				{
					name: 'typeId'
				}
			],
			proxy: {
				type: 'direct',
				api: {
					read: 'DirectApi.getOrders'
				}
			}
		});

		var orderTypesStore = Ext.create('Ext.data.Store', {
			model: 'TestMext.OrderType',
			data: [
				{id: 0, name: 'N/A'},
				{id: 1, name: 'Purchase'},
				{id: 2, name: 'Sale'},
				{id: 3, name: 'Service'}
			]
		});

		var ordersStore = Ext.create('Ext.data.Store', {
			model: 'TestMext.Order',
			autoLoad: false
		});

		cb({orderTypesStore: orderTypesStore, ordersStore: ordersStore});
	});
}