startTest(function (t) {
	var ordersStore, orderTypesStore;
	t.chain(
		function (next) {
			prepare(next);
		},
		function (next, common) {
			TestMext.Contact.setProxy({
				type: 'direct',
				api: {
					read: 'DirectApi.getContacts2'
				}
			});

			ordersStore = common.ordersStore;
			orderTypesStore = common.orderTypesStore;

			Ext.widget('grid', {
				renderTo: Ext.getBody(),
				store: ordersStore,
				columns: [
					{
						dataIndex: 'accountId',
						xtype: 'mext-associationcolumn',
						displayField: 'name',
						emptyValueText: '-'
					},
					{
						dataIndex: 'contactId',
						xtype: 'mext-associationcolumn',
						displayField: 'name',
						emptyValueText: '-'
					},
					{
						dataIndex: 'typeId',
						xtype: 'mext-associationcolumn',
						store: orderTypesStore,
						displayField: 'name',
						emptyValueText: '-'
					}
				]
			});

			ordersStore.load({
				callback: function (records, operation, success) {
					if (success)
						next();
					else
						t.fail('Cannot load orders.');
				}
			});
		},
		{waitFor: 1000},
		function (next) {
			var rec = ordersStore.first();
			rec.set('contactId', 2);
			next();
		},
		{waitFor: 1000},
		function (next) {
			t.is(t.getCell('grid', 0, 1).first().getHtml(), 'Ctc2', 'Data correctly reloaded after ID change.');
			next();
		},
		function () {
		}
	);
});
