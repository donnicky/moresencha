startTest(function (t) {
	var L = Mext.localization.Localization;
	t.ok(!!Globalize, '\'Globalize\' defined.');
	//L.setOptions({cldrDataRoot: '../build/development/TestMext/resources/more-core/cldr-data'});

	function defineLocalizedClassExample() {
		Ext.define('TestMext.LocalizedClassExample', {
			singleton: true,
			mixins: [
				'Mext.localization.Localizable'
			],
			getLocalizedName: function () {
				var L = this.L.bind(this);
				return L('name');
			}
		}, function () {
			console.debug('Load localized messages.');
			Globalize.loadMessages({
				root: {
					'TestMext.LocalizedClassExample': {
						name2: 'Name2 (root)',
						name: 'Name (root)',
						messages: {
							m1: 'm1 (root)',
							m2: 'm2 (root)'
						}
					}
				},
				ru: {
					'TestMext.LocalizedClassExample': {
						name: 'Name (ru)',
						messages: {
							m1: 'm1 (ru)'
						}
					}
				},
				'ru-BY': {
					'TestMext.LocalizedClassExample': {
						name: 'Name (ru-BY)',
						messages: {
							m2: 'm2 (ru-BY)'
						}
					}
				}
			});
		});
	}

	function defineLocalizedClassExample2() {
		Ext.define('TestMext.LocalizedClassExample2', {
			singleton: true,
			mixins: [
				'Mext.localization.Localizable'
			],
			getLocalizedName: function () {
				var L = this.L.bind(this);
				return L('name');
			}
		}, function () {
			Globalize.loadMessages({
				root: {
					'TestMext.LocalizedClassExample2': {
						name: 'Name (root)'
					}
				}
			});
			L.addLocaleData({
				ru: {
					'TestMext.LocalizedClassExample2': {
						name: 'Name (ru)'
					}
				}
			});
			L.addLocaleData(function (locale) {
				if (locale !== 'en') return null;
				return {
					en: {
						'TestMext.LocalizedClassExample2': {
							name: 'Name (en)'
						}
					}
				};
			});
			L.addLocaleData(new P(function (r) {
				r({
					fr: {
						'TestMext.LocalizedClassExample2': {
							name: 'Name (fr)'
						}
					}
				});
			}));
		});
	}

	function defineLocalizedClassExample3() {
		Ext.define('TestMext.LocalizedClassExample3', {
			mixins: [
				'Mext.localization.Localizable'
			],
			getLocalizedName: function () {
				var L = this.L.bind(this);
				return L('name');
			}
		}, function () {
			console.debug('Load localized messages.');
			Globalize.loadMessages({
				root: {
					'TestMext.LocalizedClassExample3': {
						name: 'Name',
						paramedName1: 'Name {0}',
						paramedName2: 'Name {name}'
					}
				},
				ru: {
					'TestMext.LocalizedClassExample3': {
						name: 'Имя'
					}
				}
			});
		});
	}

	function getLoadedDataNotLoadedAgainTest(locale) {
		return function (next) {
			t.firesOk({
				observable: Ext.Ajax,
				events: {
					beforerequest: 0
				},
				desc: 'Loading not started for already loaded CLDR data ({1}).'.assign(locale),
				during: function () {
					L.createLocalizer(locale).then(function () {
						next();
					});
				}
			});
		}
	}

	defineLocalizedClassExample();
	defineLocalizedClassExample2();
	defineLocalizedClassExample3();

	t.chain(
		function (next) {
			t.is(TestMext.LocalizedClassExample.getLocalizedName(), 'Name (root)', 'Message localized value is correct without locale setting.');
			next();
		},
		function (next) {
			t.diag('Default (ru-RU) locale tests');
			console.debug('Set default locale.');
			L.setDefaultLocale('ru-RU').then(function (localeDetails) {
				console.debug(localeDetails);
				t.is(L.getDefaultLocale(), 'ru-RU', 'Default locale is correct.');
				t.is(L.getCurrentLocaleDisplayName(), 'русский', 'Default locale display name is correct.');
				t.is(localeDetails.language, 'ru', 'Language of default set is correct.');
				var formattedValue = L.formatNumber(10000.35),
					expectedValue = '10{1}000,35'.assign(String.fromCharCode(160));
				console.debug(formattedValue.codes());
				console.debug(expectedValue.codes());
				t.is(formattedValue, expectedValue, 'Number correctly formatted for default (ru-RU) locale: {1}.'.assign(formattedValue));
				next();
			}).catch(function (err) {
				t.fail(err);
				console.error(err.stack);
				next();
			});
		}, function (next) {
			t.diag('Specific (en) locale tests');
			L.createLocalizer('en').then(function (en) {
				console.debug(en);
				t.is(en.cldr.locale, 'en', 'Localizator created with correct locale.');
				var formattedValue = en.formatNumber(10000.35),
					expectedValue = '10,000.35';
				console.debug(formattedValue.codes());
				console.debug(expectedValue.codes());
				t.is(formattedValue, expectedValue, 'Number correctly formatted for `en` locale: {1}.'.assign(formattedValue));
				next();
			});
		}, function (next) {
			t.diag('Multiple usage of the same locale');
			next();
		},
		getLoadedDataNotLoadedAgainTest('en'),
		getLoadedDataNotLoadedAgainTest('ru'),
		getLoadedDataNotLoadedAgainTest('ru-RU'),
		function (next) {
			t.diag('Class localization test');
			L.createLocalizer('ru-BY').then(function (localizer) {
				TestMext.LocalizedClassExample.setLocalizer(localizer);
				t.is(TestMext.LocalizedClassExample.L('name2'), 'Name2 (root)', 'Fallback to main locale works.');
				t.is(TestMext.LocalizedClassExample.getLocalizedName(), 'Name (ru-BY)', 'Message localized value is correct.');
				t.is(TestMext.LocalizedClassExample.L('messages/m1'), 'm1 (ru)', 'Fallback to main locale works.');
				t.is(TestMext.LocalizedClassExample.L('messages/m2'), 'm2 (ru-BY)', 'Message localized value is correct.');
				next();
			});
		},
		function (next) {
			t.diag('Locale data descriptors test');
			L.createLocalizer('ru').then(function (localizer) {
				TestMext.LocalizedClassExample2.setLocalizer(localizer);
				t.is(TestMext.LocalizedClassExample2.getLocalizedName(), 'Name (ru)', 'Simple object descriptor.');
			}).then(function () {
				return L.createLocalizer('en');
			}).then(function (localizer) {
				console.debug(localizer);
				TestMext.LocalizedClassExample2.setLocalizer(localizer);
				t.is(TestMext.LocalizedClassExample2.getLocalizedName(), 'Name (en)', 'Function (object) descriptor.');
			}).then(function () {
				return L.createLocalizer('fr');
			}).then(function (localizer) {
				TestMext.LocalizedClassExample2.setLocalizer(localizer);
				t.is(TestMext.LocalizedClassExample2.getLocalizedName(), 'Name (fr)', 'Promise (object) descriptor.');
			}).then(function () {
				Mext.direct.Manager.initRemoteApi('/missingrpc', 'Ext.app.REMOTING_API').then(function () {
				}).catch(function (err) {
					console.debug(err.message);
					t.ok(err.message.hasCyrillic(), 'Promise (url) descriptor.');
					next();
				});
			});
		},
		function (next) {
			t.diag('Missing localization test');
			L.createLocalizer('en-RU').then(function (localizer) {
				console.debug(localizer.cldr.attributes);
				TestMext.LocalizedClassExample.setLocalizer(localizer);
				t.is(TestMext.LocalizedClassExample.L('name2'), 'Name2 (root)', 'Fallback to root locale works.');
				console.debug(localizer.formatNumber(10000.35));
				next();
			}).catch(function (err) {
				t.fail(err);
				console.error(err.stack);
				next();
			});
		},
		function (next) {
			t.diag('Locales display names test');
			L.getLocalesInfo(['en', 'ru', 'de']).then(function (result) {
				console.debug(result);
				t.isArray(result, 'Result type is array.');
				t.is(result.length, 3, 'Array size is correct.');
				t.is(result[0].displayName, 'English', 'Display name for \'en\' is correct.');
				t.is(result[1].displayName, 'русский', 'Display name for \'ru\' is correct.');
				t.is(result[2].displayName, 'Deutsch', 'Display name for \'de\' is correct.');
				next();
			}).catch(function (err) {
				t.fail(err);
				console.error(err.stack);
				next();
			});
		},
		function (next) {
			t.diag('Available locales test');
			L.setOptions({availableLocales: ['en', 'en-GB', 'ru', 'fr-KM']});
			L.setDefaultLocale('ru-RU').then(function () {
				t.is(L.getDefaultLocale(), 'ru', 'Default locale is correct when territory is missing.');
			}).then(function () {
				return L.setDefaultLocale('fr-CA').then(function () {
					t.is(L.getDefaultLocale(), 'fr-KM', 'Default locale is correct when language exists with another territory.');
				});
			}).then(function () {
				return L.setDefaultLocale('de').then(function () {
					t.is(L.getDefaultLocale(), 'en', 'Default locale is correct when language is missing.');
				});
			}).then(function () {
				return L.setDefaultLocale('fr').then(function () {
					t.is(L.getDefaultLocale(), 'fr-KM', 'Default locale is correct when language exists with a territory.');
				});
			}).then(function () {
				L.setDefaultLocale('en-GB').then(function () {
					t.is(L.getDefaultLocale(), 'en-GB', 'Default locale is correct when locale is available.');
					next();
				});
			});
		},
		function (next) {
			t.diag('Misc');
			L.setDefaultLocale('ru').then(function () {
				t.is(TestMext.LocalizedClassExample3.L('name'), 'Имя', 'Localized resource from non-singleton class is got.');
				t.is(TestMext.LocalizedClassExample3.L('paramedName1', 'Test1'), 'Name Test1', 'Localized string with numeric param works.');
				t.is(TestMext.LocalizedClassExample3.L('paramedName2', {name: 'Test2'}), 'Name Test2', 'Localized string with named param works.');
				next();
			});
		},
		function (next) {
			document.cookie = 'locale=en; path=/';
			L.setDefaultLocale().then(function (localeDetails) {
				t.is(localeDetails.language, 'en', 'Locale correctly read from cookie.');
				next();
			});
		},
		function (next) {
			t.diag('Custom EN-150 Ext JS locale');
			L.init({availableLocales: ['en-150', 'nb']}).then(function () {
				t.is(Ext.util.Format.thousandSeparator, '.', 'Correct thousand separator.');
				t.is(Ext.util.Format.decimalSeparator, ',', 'Correct decimalSeparator separator.');
				t.is(Ext.util.Format.dateFormat, 'd.m.Y', 'Correct date format.');
				//return L.setDefaultLocale('en-150').then(function () {
				//});
			}).then(function () {
				t.diag('Custom NB Ext JS locale');
				return L.setDefaultLocale('nb').then(function () {
					t.is(Ext.Date.dayNames[1], 'Mandag', 'Translation exists.');
					t.is(Ext.util.Format.currencySign, 'kr', 'Currency sign is correct.');
				});
			}).then(function () {
				next();
			})
		},
		function () {
		}
	);
});