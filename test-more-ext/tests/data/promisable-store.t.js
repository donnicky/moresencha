startTest(function (t) {
	Ext.define('TestMext.MyPromisableStore', {
		extend: 'Ext.data.Store',
		asynchronousLoad: false,
		mixins: [
			'Mext.data.PromisableStore'
		],
		proxy: {
			type: 'ajax',
			url: 'fake.json'
		}
	});

	Ext.define('TestMext.MyPromisableTreeStore', {
		extend: 'Ext.data.TreeStore',
		mixins: [
			'Mext.data.PromisableStore'
		],
		asynchronousLoad: false,
		root: {},
		proxy: {
			type: 'direct',
			api: {
				read: 'DirectApi.getTreeStoreData'
			},
			reader: {
				type: 'json',
				rootProperty: function (node) {
					return node.data || node.children;
				}
			},
			url: 'fake.json'
		}
	});

	var store = Ext.create('TestMext.MyPromisableStore'),
		store2 = Ext.create('TestMext.MyPromisableTreeStore', {
			rootExpanded: false
		}),
		p1 = store.getLoading(),
		p2 = store2.getLoading();

	t.ok(p1.isPending(), 'Loading promise is in pending state for new store.');
	t.ok(p2.isPending(), 'Loading promise is in pending state for new tree store.');
	store.load();
	t.ok(p1 === store.getLoading(), 'Loading promise is the same after the first loading.');
	t.chain(
		function (next) {
			p1.catch(function () {
				next();
			});
		},
		function (next) {
			store.reload();
			store.getLoading().catch(function () {
			});
			t.ok(p1 !== store.getLoading(), 'Loading promise is different after reload call.');
			Mext.direct.Manager.initDefaultRemoteApi().then(function () {
				next();
			});
		},
		function (next) {
			store2.load();
			t.ok(p2 === store2.getLoading(), 'Loading promise is the same after the first loading of tree store.');
			p2.then(function () {
				next();
			});
		},
		function (next) {
			store2.reload();
			t.ok(p2 !== store2.getLoading(), 'Loading promise is different after tree store reload call.');
			store2.getLoading().then(function () {
				next();
			});
		},
		function (next) {
			store.load();
			store.getLoading().catch(function () {
				next();
			});
			store.load();
			store.getLoading().catch(function () {
			});
		},
		function (next) {
			store2.reload();
			store2.reload(); // Loading not started second time since 6.0.1.
			store2.getAllLoadings().then(function (results) {
				console.log(results);
				t.is(results.length, 1, 'Correct number of loadings returned.');
				t.isObject(results[0], 'Result is object.');
				t.ok(results[0].hasOwnProperty('records'), 'Result has records.');
				t.ok(results[0].hasOwnProperty('operation'), 'Result has operation.');
				next();
			});
		},
		function (next) {
			t.ok(store2.getLoading().isFulfilled(), 'Promise is resolved for loaded store.');
			t.ok(store2.getAllLoadings().isFulfilled(), 'Promise of all loadings is resolved for loaded store.');
			next();
		},
		function (next) {
			store.reload();
			store.reload();
			t.is(store.mextPromisable.operations.length, 2, 'Correct number of operations started.');
			store.getAllLoadings().catch(function (results) {
				next();
			});
		},
		function (next) {
			t.ok(store.getLoading().isFulfilled(), 'Promise is resolved for loaded store.');
			t.ok(store.getAllLoadings().isFulfilled(), 'Promise of all loadings is resolved for loaded store.');
			next();
		},
		function () {
		}
	);
});
