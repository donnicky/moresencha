startTest(function (t) {
	TestMext.REMOTE_API = {
		"url": "/directapi",
		"namespace": "TestMext",
		"type": "remoting",
		"actions": {
			"DirectApi": [{
				"name": "getData",
				"len": 1
			}, {
				"name": "createData",
				"len": 1
			}]
		}
	};

	Ext.direct.Manager.addProvider(TestMext.REMOTE_API);

	Ext.define('TestMext.model.JsonWriterTestModel', {
		extend: 'Ext.data.Model',

		fields: ['id', 'text'],

		proxy: {
			type: 'direct',
			api: {
				read: 'TestMext.DirectApi.getData',
				create: 'TestMext.DirectApi.createData'
			},
			reader: {
				type: 'json',
				rootProperty: 'data'
			},
			writer: {
				type: 'json',
				rootProperty: 'data'
			},
			extraParams: {
				type: 'MyModel'
			}
		}
	});

	Ext.define('TestMext.store.JsonWriterTestStore', {
		extend: 'Ext.data.Store',
		model: 'TestMext.model.JsonWriterTestModel'
	});

	var store1 = Ext.create('TestMext.store.JsonWriterTestStore', {
		autoLoad: false,
		autoSync: true
	});

	t.wait('request');

	Ext.Ajax.on('beforerequest', function (conn, options) {
		t.is(options.jsonData.data[0].type, 'MyModel', 'Extra param is included in create request');
		t.endWait('request');
	}, window, {single: true});

	store1.add(Ext.create('TestMext.model.JsonWriterTestModel'));
});