startTest(function (t) {
	var field = Ext.widget('combo', {
		renderTo: Ext.getBody(),
		queryMode: 'local',
		valueField: 'id',
		displayField: 'text',
		clearTrigger: true,
		//readOnly: true,
		store: {
			fields: ['id', 'text'],
			data: [
				{id: 1, text: 'one'},
				{id: 2, text: 'two'},
				{id: 3, text: 'three'}
			]
		},
		value: 1
	});

	t.ok(t.isElementVisible(field.el.down('.x-form-clear-trigger')), 'Clear trigger is visible after creation with value.');

	t.chain(
		function (next) {
			t.click(field.el.down('.x-form-clear-trigger'), next);
		},
		function (next) {
			t.notOk(t.isElementVisible(field.el.down('.x-form-clear-trigger')), 'Clear trigger is hidden after it was clicked.');
			next();
		},
		function (next) {
			field.setValue(2);
			t.ok(t.isElementVisible(field.el.down('.x-form-clear-trigger')), 'Clear trigger is visible after value is set.');
			next();
		},
		function (next) {
			field.setReadOnly(true);
			t.notOk(t.isElementVisible(field.el.down('.x-form-clear-trigger')), 'Clear trigger is hidden after read-only was set to true.');
			next();
		},
		function (next) {
			field.setValue(1);
			t.notOk(t.isElementVisible(field.el.down('.x-form-clear-trigger')), 'Clear trigger is still hidden after value change being read-only.');
			next();
		},
		function (next) {
			field.setReadOnly(false);
			t.ok(t.isElementVisible(field.el.down('.x-form-clear-trigger')), 'Clear trigger is visible after read-only was set to false.');
			next();
		},
		function () {
		}
	);
});
