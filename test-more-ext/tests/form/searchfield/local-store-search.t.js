startTest(function (t) {

	var store = Ext.create('Ext.data.Store', {
		fields: ['text'],
		data: [
			{text: 'Alice'},
			{text: 'Bob Alice'},
			{text: 'Bob Alice John'},
			{text: '2Alice2'},
			{text: 'Bob'}
		]
	});

	var searchField = Ext.widget('mext-searchfield', {
		renderTo: Ext.getBody(),
		filterProperty: 'text',
		store: store
	});

	t.diag('Check store filtered after type and search click');
	t.chain(
		{click: '>>mext-searchfield'},
		{action: 'type', text: 'alice'},
		{click: '.x-form-search-trigger'},
		function (next) {
			t.is(store.getCount(), 4, 'Store correctly filtered.');
			t.diag('Check store filter reset after text deletion and ENTER');
			next();
		},
		{click: '>>mext-searchfield'},
		{action: 'type', text: '[BACKSPACE][BACKSPACE][BACKSPACE][BACKSPACE][BACKSPACE][ENTER]'},
		function (next) {
			t.is(store.getCount(), 5, 'Store filter is reset.');
			t.diag('Check store filtered after type and ENTER');
			next();
		},
		{action: 'type', text: 'alice[ENTER]'},
		function (next) {
			t.is(store.getCount(), 4, 'Store correctly filtered.');
			t.diag('Check store filter reset after text deletion and blur');
			next();
		},
		{action: 'type', text: '[BACKSPACE][BACKSPACE][BACKSPACE][BACKSPACE][BACKSPACE][TAB]'},
		function (next) {
			t.is(store.getCount(), 5, 'Store filter is reset.');
			t.diag('Check store filtered after type and TAB');
			next();
		},
		{click: '>>mext-searchfield'},
		{action: 'type', text: 'alice[TAB]'},
		function (next) {
			t.is(store.getCount(), 4, 'Store correctly filtered.');
			next();
		},
		function () {
		}
	);
});