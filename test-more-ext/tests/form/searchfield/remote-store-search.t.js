startTest(function (t) {
	var store = Ext.create('Ext.data.Store', {
		fields: ['text'],
		proxy: {
			type: 'ajax',
			url: 'fake.json'
		},
		remoteFilter: true
	});

	Ext.Ajax.on('beforerequest', function (conn, options) {
		t.diag('Checking request params');
		var filterParam = options.params.filter;
		t.ok(filterParam != null, 'Request contains filter param.');
		var filter = JSON.parse(filterParam);
		t.ok(filter instanceof Array && filter.length === 1, 'Request contains one filter.');
		filter = filter[0];
		t.is(filter.property, 'text', 'Request has correct filter property name.');
		t.is(filter.value, 'alice', 'Request has correct filter property value.');
		t.is(filter.operator, 'like', 'Request has correct filter operator.');
	});

	var searchField = Ext.widget('mext-searchfield', {
		renderTo: Ext.getBody(),
		filterProperty: 'text',
		store: store,
		value: 'alice'
	});

	t.chain(
		function () {
			searchField.onSearchClick();
			//t.is(store.getCount(), 4, 'Correct AJAX params sent.');
		}
	);
});