startTest(function (t) {
	t.wait('gettingResourcesPath');
	Mext.helpers.ResourcesManager.ensurePath().then(function (path) {
		t.is(path, 'http://localhost:52489/build/development/TestMext/resources', 'Resources path: {1}'.assign(path))
	}).catch(function () {
		t.fail('Could not define resources path.');
	}).finally(function () {
		t.endWait('gettingResourcesPath');
	});
});