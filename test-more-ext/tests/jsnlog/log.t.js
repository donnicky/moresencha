startTest(function (t) {
	var beforeXmlHttpRequest;
	(function (open) {
		XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {
			if (beforeXmlHttpRequest) beforeXmlHttpRequest.resolve({method: method, url: url});
			open.call(this, method, url, async, user, pass);
		};
	})(XMLHttpRequest.prototype.open);

	var log = Ext.create('Mext.log.Log', {
		level: JL.getFatalLevel()
	});
	log.setLevel(JL.getTraceLevel());
	log.setAjax(true);
	log.setAjaxLevel(JL.getTraceLevel());
	t.ok(!!log._ajaxAppender, 'Ajax appender defined.');
	t.is(log._ajaxAppender.level, JL.getTraceLevel(), 'Ajax appender has TRACE level.');
	t.chain(
		function (next) {
			beforeXmlHttpRequest = Promise.pending();
			log.info({msg: 'Hi!'});
			beforeXmlHttpRequest.promise.then(function (request) {
				t.is(request.url, '/jsnlog', 'Server info call logged.');
				next();
			});
		},
		function (next) {
			try {
				throw new Error('test');
			}
			catch (err) {
				log.fatalException({
					errorMsg: 'test',
					url: 'url',
					lineNumber: 1,
					columnNumber: 1
				}, err);
				next();
			}
		},
		function (next) {
			try {
				throw 'test2';
			}
			catch (err) {
				log.fatalException({
					errorMsg: 'test',
					url: 'url',
					lineNumber: 1,
					columnNumber: 1
				}, err);
				next();
			}
		},
		function () {
		}
	);
});