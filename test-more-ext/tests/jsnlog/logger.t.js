startTest(function (t) {
	var beforeXmlHttpRequest;
	(function (open) {
		XMLHttpRequest.prototype.open = function (method, url, async, user, pass) {
			if (beforeXmlHttpRequest) beforeXmlHttpRequest.resolve({method: method, url: url});
			open.call(this, method, url, async, user, pass);
		};
	})(XMLHttpRequest.prototype.open);

	Mext.log.Logger.init({log: {ajax: true}});

	t.chain(
		function (next) {
			beforeXmlHttpRequest = Promise.pending();
			beforeXmlHttpRequest.promise.then(function (request) {
				t.is(request.url, '/jsnlog', 'Fatal exception logged to server.');
				next();
			});

			new Promise(function () {
				throw new Error('Test');
			});
		},
		function (next) {
			beforeXmlHttpRequest = Promise.pending();
			beforeXmlHttpRequest.promise.then(function (request) {
				t.is(request.url, '/jsnlog', 'Fatal exception logged to server.');
				next();
			});

			new Promise(function (resolve, reject) {
				reject('test1');
			});
		},
		function (next) {
			beforeXmlHttpRequest = Promise.pending();
			beforeXmlHttpRequest.promise.then(function (request) {
				t.is(request.url, '/jsnlog', 'Fatal exception logged to server.');
				next();
			});

			new Promise(function () {
				Ext.raise('Test123');
			});
		},
		function (next) {
			beforeXmlHttpRequest = Promise.pending();
			beforeXmlHttpRequest.promise.then(function () {
				t.fail('Handled exception should not be logged to server.');
			});

			new Promise(function (resolve, reject) {
				reject('test1');
			}).catch(function () {
					next();
				});
		},
		function () {
		}
	);
});