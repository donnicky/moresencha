// Wrong data api called after record edit in tree panel
// https://www.sencha.com/forum/showthread.php?292957
startTest(function (t) {
	Ext.define('MegaRoot', {
		extend: 'Ext.data.TreeModel',
		childType: 'RootElement',
		proxy: {
			type: 'ajax',
			api: {
				update: 'https://aaaaaa/UpdateMegaRootElement'
			}
		}
	});

	Ext.define('RootElement', {
		extend: 'Ext.data.TreeModel',
		childType: 'ChildElement',
		proxy: {
			type: 'ajax',
			api: {
				update: 'https://aaaaaa/UpdateRootElement'
			}
		}
	});

	Ext.define('ChildElement', {
		extend: 'Ext.data.TreeModel',
		proxy: {
			type: 'ajax',
			api: {
				update: 'https://aaaaaa/UpdateChildElement'
			}
		}
	});

	Ext.define('MyStore', {
		extend: 'Ext.data.TreeStore',
		model: 'MegaRoot',
		autoSync: true,
		root: {
			children: [{
				id: 1,
				text: 'RootElement1',
				descr: 'RootElement1',
				expanded: true,
				children: [{
					id: 2,
					text: 'ChildElement1',
					descr: 'ChildElement1',
					leaf: true
				}, {
					id: 3,
					text: 'ChildElement2',
					descr: 'ChildElement2',
					leaf: true
				}]
			}]
		}
	});

	var store = Ext.create('MyStore'),
		url;
	t.chain(
		function (next) {
			Ext.Ajax.on('beforerequest', function (conn, options) {
				url = options.url;
				next();
			});
			store.getNodeById(2).set('descr', 'test');
		},
		function(next) {
			t.knownBugIn('6.0.1.250', function (t) {
				t.like(url, 'UpdateChildElement', 'Correct API is called.');
			}, 'extjs-15457');
			next();
		},
		function() {
		}
	);
});