// EXTJS-15512. Strange cellediting plugin events firing order.
// https://www.sencha.com/forum/showthread.php?293554
startTest(function (t) {
	Ext.application({
		name: 'Fiddle',

		launch: function () {
			startFiddleApplication();
		}
	});

	var grid;
	var events = [];

	t.chain(
		function (next) {
			t.waitForCQVisible('grid', function (result) {
				grid = result[0];
				grid.on('beforeedit', function (editor, context) {
					events.push('beforeedit:'+context.column.dataIndex);
				});
				grid.on('edit', function (editor, context) {
					events.push('edit:'+context.column.dataIndex);
				});
				next();
			});
		},
		function (next) {
			t.clickToEditCell(grid, 0, 0, next)
		},
		function (next, inputEl) {
			t.type(inputEl, '[TAB]', next)
		},
		function (next) {
			t.clickToEditCell(grid, 0, 1, next)
		},
		function (next, inputEl) {
			t.type(inputEl, '[ENTER]', next)
		},
		function(next) {
			t.knownBugIn('6.0.1.250', function (t) {
				t.is(events[1], 'edit:id', 'Events firing order is correct');
			});
			next();
		}
	)
});