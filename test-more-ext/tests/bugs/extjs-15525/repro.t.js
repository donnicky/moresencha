// Ext.selection.Model does not correctly update ids of phantom records
// https://www.sencha.com/forum/showthread.php?299966
startTest(function (t) {
	var treePanel, insertedNode;

	t.chain(
		function (next) {
			t.waitForCQVisible('treepanel', function (result) {
				treePanel = result[0];
				next();
			});
		},
		function (next) {
			treePanel.getStore().on('nodeinsert', function (store, node) {
				insertedNode = node;
			}, treePanel, {single: true});
			next();
		},
		function (next) {
			t.clickCQ('button[text=Add record]', next);
		},
		function (next) {
			t.waitForFn(function () {
				return insertedNode && insertedNode.get('text') === 'RootElement2';
			}, next);
		},
		function (next) {
			t.knownBugIn('5.1.0.107', function(t) {
				t.hasNotCls(treePanel.getView().getNode(insertedNode), 'x-grid-item-selected', 'Inserted node should not be selected.');
				treePanel.getSelectionModel().select(treePanel.getStore().getAt(1));
				t.firesOnce(treePanel, 'select', 'Inserted node should be selectable.');
				treePanel.getSelectionModel().select(insertedNode);
			}, 'extjs-15525');
			next();
		},
		function () {

		}
	);
});