// Nested data not loaded correctly during update operation
// https://www.sencha.com/forum/showthread.php?291474
startTest(function (t) {
	Ext.define('TestMext.Order', {
		extend: 'Ext.data.Model',
		fields: ['id', 'name', 'comment'],
		proxy: {
			type: 'ajax',
			api: {
				update: 'tests/bugs/extjs-15037/updateOrder.json',
				read: 'tests/bugs/extjs-15037/getOrder.json'
			},
			reader: {
				type: 'json'
			}
		}
	});

	Ext.define('TestMext.OrderItem', {
		extend: 'Ext.data.Model',
		fields: ['id', {
			name: 'orderId',
			reference: {
				type: 'TestMext.Order',
				inverse: 'orderItems'
			}
		}, 'qty']
	});

	var orders = Ext.create('Ext.data.Store', {
		model: 'TestMext.Order'
	});

	var updateQty;

	t.chain(
		function(next) {
			orders.load(function() {
				orders.getAt(0).set('name', 'Order #1');
				orders.sync({
					success: function() {
						console.debug(orders.getAt(0).get('comment'));
						updateQty = orders.getAt(0).orderItems().getAt(0).get('qty');
						// qty should be 2 as provided in the update response nested data
						console.debug(updateQty);
						next();
					}
				});
			});
		},
		function(next) {
			t.knownBugIn('6.0.1.250', function(t) {
				t.is(updateQty, 2, 'Order item qty should be update from server response.');
			}, 'extjs-15037');
			next();
		},
		function() {}
	);
});