// Wrong model class for associated records
// https://www.sencha.com/forum/showthread.php?295904
startTest(function (t) {
	Ext.define('MegaRoot', {
		extend: 'Ext.data.TreeModel',
		childType: 'RootElement'
	});

	Ext.define('RootElement', {
		extend: 'Ext.data.TreeModel',
		childType: 'ChildElement'
	});

	Ext.define('ChildElement', {
		extend: 'Ext.data.TreeModel',
		fields: [{
			name: 'nestedElementId',
			reference: 'NestedElement'
		}]
	});

	Ext.define('NestedElement', {
		extend: 'Ext.data.TreeModel',
		fields: ['id', 'text']
	});

	Ext.define('MyStore', {
		extend: 'Ext.data.TreeStore',
		model: 'MegaRoot',
		autoSync: true,
		root: {
			children: [{
				id: 1,
				text: 'RootElement1',
				descr: 'RootElement1',
				expanded: true,
				children: [{
					id: 2,
					text: 'ChildElement1',
					descr: 'ChildElement1',
					nestedElementId: 1,
					nestedElement: {
						id: 1,
						text: 'ne1'
					},
					leaf: true
				}, {
					id: 3,
					text: 'ChildElement2',
					descr: 'ChildElement2',
					nestedElementId: 2,
					nestedElement: {
						id: 2,
						text: 'ne2'
					},
					leaf: true
				}]
			}]
		}
	});

	var store = Ext.create('MyStore');
	var clsName = store.getNodeById(2).getNestedElement().$className;
	t.is(clsName, 'NestedElement', 'Class name for nested element is correct.');

});