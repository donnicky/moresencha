// 2nd Paged Combo without Id fails to "Drop" due to duplicate Id registration
// https://www.sencha.com/forum/showthread.php?303101
startTest(function (t) {
	var combos;

	startFiddleApplication();

	t.chain(
		function (next) {
			t.waitForCQVisible('combobox', function (result) {
				combos = result;
				next();
			});
		},
		function (next) {
			t.click('combobox[fieldLabel=Choose State] => .x-form-trigger', next);
		},
		function (next) {
			t.click('li:contains(Alaska)', next);
		},
		function (next) {
			t.click('combobox[fieldLabel=Drop me Im Broken] => .x-form-trigger', next);
		},
		function () {
		}
	);
});