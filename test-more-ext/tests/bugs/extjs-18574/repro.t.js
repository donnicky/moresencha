// 2nd Paged Combo without Id fails to "Drop" due to duplicate Id registration
// https://www.sencha.com/forum/showthread.php?303101
startTest(function (t) {
	var combos;

	Ext.application({
		name: 'Fiddle',

		launch: function () {
			startFiddleApplication();
		}
	});

	t.chain(
		function (next) {
			t.waitForCQVisible('combobox', function (result) {
				combos = result;
				next();
			});
		},
		function (next) {
			t.click('combobox[fieldLabel=Choose State] => .x-form-trigger', next);
		},
		function (next) {
			t.click('li:contains(Alaska)', next);
		},
		function (next) {
			var errorCaught = false;
			window.onerror = function (errMsg) {
				var expected = 'Uncaught Error: Registering duplicate component id "undefined-paging-toolbar"';
				t.knownBugIn('6.0.1.250', function (t) {
					t.isNot(errMsg, expected);
				});
				errorCaught = true;
				setTimeout(next);
				if (errMsg !== expected) {
					t.fail(errMsg);
					return false;
				}
				return true;

			};
			t.click('combobox[fieldLabel=Drop me Im Broken] => .x-form-trigger');
			setTimeout(function () {
				if (!errorCaught)
					next();
			}, 3000);
		},
		function () {
		}
	);
});