startTest(function (t) {
	t.diag('General test');
	var dlg = Ext.widget('mext-logindlg', {
		autoShow: true,
		authStatusUrl: '/authmsg'
	});
	console.debug(dlg.getTitle());
	t.is(dlg.getTitle(), 'Log in', 'Default title is set correctly.');
	t.elementIsNotVisible(dlg.down('#localeSelector'), 'Locale selector is hidden when no locales provided.');
	dlg.close();

	t.chain(
		function (next) {
			t.diag('Dialog #1 test');
			dlg = Ext.widget('mext-logindlg', {
				title: 'Test',
				autoShow: true,
				authStatusUrl: '/authmsg',
				locales: ['en', 'ru']
			});
			console.debug(dlg.getTitle());
			t.is(dlg.getTitle(), 'Test', 'Custom title is set correctly.');
			t.elementIsVisible(dlg.down('#localeSelector'), 'Locale selector is visible when locales provided.');
			dlg.on('localesload', function () {
				t.is(dlg.down('#localeSelector').getText(), 'English', 'Locale selector\'s text is correct.');
				dlg.close();
				next();
			});
		},
		function (next) {
			t.diag('Dialog #2 test');
			Mext.localization.Localization.setDefaultLocale('no').then(function () {
				dlg = Ext.widget('mext-logindlg', {
					title: 'Test',
					autoShow: true,
					authStatusUrl: '/authmsg',
					locales: ['en', 'ru']
				});
				dlg.on('localesload', function () {
					t.is(dlg.down('#localeSelector').getText(), 'no', 'Locale selector\'s text is correct for missing bundle.');
					t.is(dlg.localeSelector.getMenu().items.length, 2, 'Menu has correct number of items.');
					dlg.close();
					next();
				});
			});
		},
		function (next) {
			t.diag('Dialog #3 test');
			Mext.localization.Localization.setOptions({availableLocales: ['en', 'en-GB', 'ru', 'de', 'fr', 'it']});
			Mext.localization.Localization.setDefaultLocale('en-US').then(function () {
				dlg = Ext.widget('mext-logindlg', {
					autoShow: true,
					authStatusUrl: '/authmsg'
				});
				dlg.on('localesload', function () {
					t.is(dlg.down('#localeSelector').getText(), 'English', 'Another locale selector\'s text is correct.');
					t.is(dlg.localeSelector.getMenu().items.length, 6, 'Menu has correct number of items.');
					t.is(dlg.localeSelector.getMenu().items.getAt(0).checked, true, 'Current locale is checked in menu.');
					dlg.close();
					next();
				});
			});
		},
		function (next) {
			t.diag('Dialog #4 test. Custom toolbar items.');
			dlg = Ext.widget('mext-logindlg', {
				autoShow: true,
				authStatusUrl: '/authmsg',
				moreToolbarItems: [
					{
						xtype: 'button',
						text: 'ThemeSelect'
					},
					{
						xtype: 'button',
						text: 'OneMoreButton'
					}
				]
			});
			t.ok(dlg.down('button[text=ThemeSelect]'), 'Additional component #1 exists in dialog\'s toolbar');
			t.ok(dlg.down('button[text=OneMoreButton]'), 'Additional component #2 exists in dialog\'s toolbar');
			next();
		},
		function () {
		}
	);
});
