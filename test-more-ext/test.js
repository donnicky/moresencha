var Harness,
	isNode = typeof process != 'undefined' && process.pid;

if (isNode) {
	Harness = require('siesta');
} else {
	Harness = Siesta.Harness.Browser.ExtJS;
}

Harness.configure({
	title: 'Test Mext',
	expectedGlobals: ['TestMext', 'Mext', 'Mcore', 'DirectApi', '$', 'jQuery', 'P', 'Cldr', 'Globalize', 'Promise', '__extends', 'JL', 'define', 'exports', 'prepare'],
	autoCheckGlobals: true,
	viewDOM: true,
	//waitForAppReady: true,
	preload: [
		'bootstrap.js'
	]
});

Harness.start(
	{
		group: 'form',
		items: [
			{
				group: 'combobox',
				items: [
					'tests/form/combobox/clear-trigger.t.js'
				]
			},
			{
				group: 'searchfield',
				items: [
					'tests/form/searchfield/local-store-search.t.js',
					'tests/form/searchfield/remote-store-search.t.js'
				]
			}
		]
	},
	{
		group: 'grid',
		items: [
			{
				group: 'associationcolumn',
				runCore: 'sequential',
				items: [
					{
						url: 'tests/grid/associationcolumn/default-renderers.t.js',
						preload: [
							'bootstrap.js',
							'tests/grid/associationcolumn/common.js'
						]
					},
					{
						url: 'tests/grid/associationcolumn/custom-renderers.t.js',
						preload: [
							'bootstrap.js',
							'tests/grid/associationcolumn/common.js'
						]
					},
					{
						url: 'tests/grid/associationcolumn/editing.t.js',
						preload: [
							'bootstrap.js',
							'tests/grid/associationcolumn/common.js'
						]
					}
				]
			}
		]
	},
	{
		group: 'dialogs',
		items: [
			'tests/dialogs/login.t.js'
		]
	},
	{
		group: 'direct api',
		runCore: 'sequential',
		items: [
			'tests/direct-api/direct-api-manager.t.js',
			'tests/direct-api/state-direct-provider.t.js',
			'tests/direct-api/direct-api-concurrency.t.js'
		]
	},
	{
		group: 'data',
		items: [
			'tests/data/json-writer-override.t.js',
			'tests/data/promisable-store.t.js'
		]
	},
	{
		group: 'jsnlog',
		items: [
			'tests/jsnlog/log.t.js',
			'tests/jsnlog/logger.t.js'
		]
	},
	'tests/resources-manager.t.js',
	'tests/localization.t.js',
	{
		group: 'bugs',
		items: [
			{
				group: 'extjs-15525',
				items: [
					{
						transparentEx: true, // When set to true harness will not try to catch any exception, thrown from the test code.
						autoCheckGlobals: false,
						waitForAppReady: false,
						url: 'tests/bugs/extjs-15525/repro.t.js',
						preload: [
							'../ext/build/classic/theme-classic/resources/theme-classic-all.css',
							'../ext/build/ext-all-debug.js',
							'fiddles/extjs-15525/index.js'
						]
					}

				]
			},
			{
				group: 'extjs-15037',
				items: [
					{
						transparentEx: true,
						autoCheckGlobals: false,
						waitForAppReady: false,
						url: 'tests/bugs/extjs-15037/repro.t.js',
						preload: [
							'../ext/build/classic/theme-classic/resources/theme-classic-all.css',
							'../ext/build/ext-all-debug.js'
						]
					},
					{
						transparentEx: true,
						url: 'tests/bugs/extjs-15037/fix.t.js'
					}
				]
			},
			{
				group: 'extjs-16282',
				items: [
					{
						transparentEx: true,
						autoCheckGlobals: false,
						waitForAppReady: false,
						url: 'tests/bugs/extjs-16282/repro.t.js',
						preload: [
							'../ext/build/classic/theme-classic/resources/theme-classic-all.css',
							'../ext/build/ext-all-debug.js'
						]
					},
					{
						transparentEx: true,
						autoCheckGlobals: false,
						url: 'tests/bugs/extjs-16282/fix.t.js'
					}
				]
			},
			{
				group: 'extjs-15457',
				items: [
					{
						transparentEx: true,
						autoCheckGlobals: false,
						waitForAppReady: false,
						url: 'tests/bugs/extjs-15457/repro.t.js',
						preload: [
							'../ext/build/classic/theme-classic/resources/theme-classic-all.css',
							'../ext/build/ext-all-debug.js'
						]
					},
					{
						transparentEx: true,
						autoCheckGlobals: false,
						url: 'tests/bugs/extjs-15457/fix.t.js'
					}
				]
			},
			{
				group: 'extjs-18574',
				runCore: 'sequential',
				items: [
					{
						transparentEx: true,
						autoCheckGlobals: false,
						waitForAppReady: false,
						url: 'tests/bugs/extjs-18574/repro.t.js',
						preload: [
							'../ext/build/classic/theme-classic/resources/theme-classic-all.css',
							'../ext/build/ext-all-debug.js',
							'fiddles/extjs-18574/index.js'
						]
					},
					{
						transparentEx: true,
						autoCheckGlobals: false,
						url: 'tests/bugs/extjs-18574/fix.t.js',
						preload: [
							'bootstrap.js',
							'fiddles/extjs-18574/index.js'
						]
					}
				]
			},
			{
				group: 'extjs-15512',
				runCore: 'sequential',
				items: [
					{
						transparentEx: true,
						autoCheckGlobals: false,
						waitForAppReady: false,
						url: 'tests/bugs/extjs-15512/repro.t.js',
						preload: [
							'../ext/build/classic/theme-classic/resources/theme-classic-all.css',
							'../ext/build/ext-all-debug.js',
							'fiddles/extjs-15512/index.js'
						]
					}
				]
			}
		]
	}
);