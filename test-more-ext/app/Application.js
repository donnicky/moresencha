/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('TestMext.Application', {
	extend: 'Ext.app.Application',
	name: 'TestMext',
	requires: [
		'Mext.*',
		'Ext.data.*',
		'Ext.grid.*'
	],

	stores: [
		// TODO: add global / shared stores here
	],

	launch: function () {
		//Mext.log.Logger.init({log: {ajax: true}});
		//Mext.log.Logger.setShowAlert(false);
		//this.testLoggable();
		//this.testLoginDlg();
		//this.testExceptionsHandling();
	},

	testLoggable: function () {
		Ext.define('TestMext.TestLoggable', {
			mixins: [
				'Mext.log.Loggable'
			],
			autoLogMethods: true,
			someMethod: function () {
				console.log('someMethod() called');
				for (var i = 0; i < 10000; i++) {
					for (var j = 0; j < 10000; j++) {
					}
				}
				this.getLog().info('Some operation is going to happen.');
				this.logOperation('someOperation', function () {
				});
			},
			someMethod2: function() {
			},
			statics: {
				someStaticMethod: function () {
				}
			},
			inheritableStatics: {
				someInheritableStaticMethod: function () {
				}
			}
		});

		var a = Ext.create('TestMext.TestLoggable');
		a.someMethod();
		a.someMethod2();
	},

	testExceptionsHandling: function () {
		Mext.log.Logger.setShowAlert(true);

		//throw 'Test error'; // never do this

		//throw new Error('Test error');

		//Ext.raise('Test error'); // previous is better, because Ext.raise makes stack more dirty and duplicates console output in debug mode.

		//Ext.raise(new Error('Test error')); // never do this

		//new Promise(function () {
		//	throw new Error('Test error');
		//});

		throw new Mext.error.DestroyedObjectAccessed('test');
	},

	testLoginDlg: function () {
		Ext.widget('mext-logindlg', {
			autoShow: true,
			authStatusUrl: '/authmsg',
			moreToolbarItems: [
				{
					xtype: 'button',
					text: 'ThemeSelect'
				},
				{
					xtype: 'button',
					text: 'OneMoreButton'
				}
			]
		});
	}
});
