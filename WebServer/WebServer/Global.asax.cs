﻿namespace WebServer
{
	using System;
	using System.Globalization;

	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");
			var metaData = new ExtDirectHandler.Configuration.ReflectionConfigurator().RegisterType<DirectApi>();
			ExtDirectHandler.DirectHttpHandler.SetMetadata(metaData);
		}

		protected void Session_Start(object sender, EventArgs e)
		{
		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			////if (Context.Request.Path.Contains("rpc"))
			////	Context.SetSessionStateBehavior(SessionStateBehavior.ReadOnly);
		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}

		protected void Application_Error(object sender, EventArgs e)
		{
		}

		protected void Session_End(object sender, EventArgs e)
		{
		}

		protected void Application_End(object sender, EventArgs e)
		{
		}
	}
}