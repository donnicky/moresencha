﻿namespace WebServer
{
	using System;
	using System.Globalization;
	using System.IO;
	using System.Threading;
	using System.Web;

	using TestMoreExt;

	public class StaticContentHandler : IHttpHandler
	{
		/// <summary>
		/// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler" /> instance.
		/// </summary>
		/// <value><c>true</c> if this instance is reusable; otherwise, <c>false</c>.</value>
		public bool IsReusable
		{
			get { return true; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler" /> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext" /> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		/// <exception cref="HttpException">The Web application is running under IIS 7 in Integrated mode.</exception>
		/// <exception cref="DirectoryNotFoundException">The specified path is invalid, (for example, it is on an unmapped drive). </exception>
		/// <exception cref="UnauthorizedAccessException">specified a directory.-or- The caller does not have the required permission. </exception>
		/// <exception cref="FileNotFoundException">The file specified was not found. </exception>
		/// <exception cref="IOException">An I/O error occurred.</exception>
		public void ProcessRequest(HttpContext context)
		{
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
			var absolutePath = context.Request.Url.AbsolutePath;
			var physicalApplicationPath = context.Request.PhysicalApplicationPath;

			if (physicalApplicationPath == null)
			{
				context.Response.StatusCode = 500;
				return;
			}

			var physicalFilePath = Path.Combine(physicalApplicationPath, "..", "..", absolutePath.Substring(1));
			if (absolutePath == "/" || absolutePath == "/index.html")
				physicalFilePath = Path.Combine(physicalApplicationPath, "index.html");
			context.Response.ContentType = MimeTypes.GetMimeType(physicalFilePath);

			if (!File.Exists(physicalFilePath))
			{
				context.Response.StatusCode = 404;
				return;
			}

			using (var sourceStream = File.OpenRead(physicalFilePath))
			{
				sourceStream.CopyTo(context.Response.OutputStream);
			}
		}
	}
}