﻿namespace WebServer
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Threading;
	using System.Threading.Tasks;
	using System.Web;

	using ExtDirectHandler.Configuration;

	using Newtonsoft.Json.Linq;

	public class DirectApi
	{
		private static readonly Dictionary<string, string> State = new Dictionary<string, string>();
		private static readonly ReaderWriterLockSlim StateLock = new ReaderWriterLockSlim();

		/// <exception cref="LockRecursionException">The <see cref="P:System.Threading.ReaderWriterLockSlim.RecursionPolicy" /> property is <see cref="F:System.Threading.LockRecursionPolicy.NoRecursion" /> and the current thread has already entered read mode. -or-The recursion number would exceed the capacity of the counter. This limit is so large that applications should never encounter it.</exception>
		/// <exception cref="SynchronizationLockException">The current thread has not entered the lock in read mode.</exception>
		public Dictionary<string, string> GetState()
		{
			StateLock.EnterReadLock();
			try
			{
				return State;
			}
			finally
			{
				StateLock.ExitReadLock();
			}
		}

		/// <exception cref="LockRecursionException">The <see cref="P:System.Threading.ReaderWriterLockSlim.RecursionPolicy" /> property is <see cref="F:System.Threading.LockRecursionPolicy.NoRecursion" /> and the current thread has already entered the lock in any mode. -or-The current thread has entered read mode, so trying to enter the lock in write mode would create the possibility of a deadlock. -or-The recursion number would exceed the capacity of the counter. The limit is so large that applications should never encounter it.</exception>
		/// <exception cref="SynchronizationLockException">The current thread has not entered the lock in write mode.</exception>
		public void ClearState(string key)
		{
			StateLock.EnterWriteLock();
			try
			{
				if (State.ContainsKey(key)) State.Remove(key);
			}
			finally
			{
				StateLock.ExitWriteLock();
			}
		}

		/// <exception cref="LockRecursionException">The <see cref="P:System.Threading.ReaderWriterLockSlim.RecursionPolicy" /> property is <see cref="F:System.Threading.LockRecursionPolicy.NoRecursion" /> and the current thread has already entered the lock in any mode. -or-The current thread has entered read mode, so trying to enter the lock in write mode would create the possibility of a deadlock. -or-The recursion number would exceed the capacity of the counter. The limit is so large that applications should never encounter it.</exception>
		/// <exception cref="SynchronizationLockException">The current thread has not entered the lock in write mode.</exception>
		public void SetState(JObject args)
		{
			var key = args.Value<string>("key");
			var value = args.Value<string>("value");

			StateLock.EnterWriteLock();
			try
			{
				if (State.ContainsKey(key)) State[key] = value;
				else State.Add(key, value);
			}
			finally
			{
				StateLock.ExitWriteLock();
			}
		}

		public void LongRunningMethod1()
		{
			TestSession(HttpContext.Current);
			Thread.Sleep(3000);
		}

		public void LongRunningMethod2()
		{
			TestSession(HttpContext.Current);
			Thread.Sleep(3000);
		}

		/// <summary>
		/// Gets the contacts.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns></returns>
		/// <exception cref="InvalidOperationException">Test error</exception>
		[DirectMethod(NamedArguments = true)]
		public IEnumerable GetContacts(int id)
		{
			switch (id)
			{
				case 1:
					return new[] { new { Id = 1, Name = "Ctc1" } };
				case 3:
					throw new InvalidOperationException("Test error");
			}

			return new object[0];
		}

		[DirectMethod(NamedArguments = true)]
		public IEnumerable GetContacts2(int id)
		{
			switch (id)
			{
				case 1:
					return new[] { new { Id = 1, Name = "Ctc1" } };
				case 2:
					return new[] { new { Id = 2, Name = "Ctc2" } };
				case 3:
					return new[] { new { Id = 3, Name = "Ctc3" } };
			}

			return new object[0];
		}

		public IEnumerable GetOrders()
		{
			return new[]
				{
					new { Id = 1, AccountId = (int?)1, Account = new { Id = 1, Name = "Acc1" }, ContactId = 1, TypeId = 1 },
					new { Id = 2, AccountId = (int?)2, Account = new { Id = 2, Name = "Acc2" }, ContactId = 2, TypeId = 2 },
					new { Id = 3, AccountId = (int?)3, Account = new { Id = 3, Name = "Acc3" }, ContactId = 3, TypeId = 0 },
					new { Id = 4, AccountId = (int?)null, Account = new { Id = 0, Name = string.Empty }, ContactId = 0, TypeId = 3 }
				};
		}

		public async void LongRunningAsyncMethod1()
		{
			var httpContext = HttpContext.Current;
			await Task.Run(() =>
				{
					TestSession(httpContext);
					Thread.Sleep(3000);
				});
		}

		public object GetTreeStoreData()
		{
			return new
			{
				Data = new object[]
					{
						new
						{
							Id = 1,
							Text = "Test1",
							Children = new object[]
								{
									new
									{
										ParentId = 1,
										Id = 2,
										Text = "Test2"
									}
								}
						}
					}
			};
		}

		private static void TestSession(HttpContext httpContext)
		{
			// HttpContext.Current is null if used from another thread.
			if (httpContext == null)
			{
				Debug.WriteLine("HttpContext.Current = null");
				return;
			}

			if (httpContext.Session["A123"] == null) httpContext.Session["A123"] = 0;
			var value = (int)httpContext.Session["A123"];
			value++;
			httpContext.Session["A123"] = value;
			Debug.WriteLine(httpContext.Session["A123"]);
		}
	}
}