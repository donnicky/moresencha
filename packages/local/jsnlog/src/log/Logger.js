/**
 * Global errors logger.
 */
Ext.define('Mext.log.Logger', {
	requires: [
		'Mext.log.Log'
	],
	singleton: true,

	config: {
		/**
		 * Shows alert window on exception if `true`.
		 */
		showAlert: true
	},

	constructor: function () {
		this.initConfig();
		return this.callParent();
	},

	/**
	 * Initializes global error handlers.
	 * @param options Options.
	 * @returns {Mext.log.Logger} This instance.
	 */
	init: function (options) {
		if (this._isInitialized) return this;
		options = options || {};
		var me = this,
			logConfig = options.log || {};
		me._log = Ext.create('Mext.log.Log', Ext.apply({
			console: false,
			name: 'GlobalErrors'
		}, logConfig));
		window.addEventListener('error', me.globalErrorListener.bind(me));
		window.addEventListener('unhandledrejection', me.globalUnhandledRejectionListener.bind(me));
		me._isInitialized = true;
		return me;
	},

	destroy: function () {
		window.removeEventListener('error', this.globalErrorListener.bind(this));
		window.removeEventListener('unhandledrejection', this.globalUnhandledRejectionListener.bind(this));
		this._log.destroy();
		this._log = null;
		this.callParent();
	},

	globalErrorListener: function (e) {
		//noinspection JSUnusedAssignment
		var preventDefault = true;
		//<debug>
		preventDefault = false;
		//</debug>
		if (preventDefault)
			e.preventDefault();

		var me = this,
			error = e['error'],
			errorDetails = {
				errorMsg: e['message'],
				url: e['filename'],
				lineNumber: e['lineno'],
				columnNumber: e['colno']
			};

		me._log.fatalException(errorDetails, error);

		if (me.getShowAlert())
			me.showException(errorDetails, error);

		//if (error instanceof EmptyError || error && error.constructor && error.constructor.name === 'EmptyError') return true;
		//
		//if (typeof error === 'object' && error && error.constructor && error.constructor.name.match('Error$')) {
		//	me.fatalException(errorMsg, error);
		//	me.showException(error.message, error.stack);
		//}
		//else {
		//	me.fatal(errorMsg);
		//	me.showException(errorMsg);
		//}
	},

	showException: function (errorDetails, error) {
		if (error instanceof Error) {
			Ext.Msg.show({
				title: error.name,
				message: error.message + '<br/>Stack trace: ' + error.stack.split('\n').join('<br/>'),
				icon: Ext.Msg.ERROR
			});
		} else {
			Ext.Msg.show({
				title: 'Error',
				message: errorDetails.errorMsg + '<br/>' + errorDetails.url + ':' + errorDetails.lineNumber + ':' + errorDetails.columnNumber,
				icon: Ext.Msg.ERROR
			});
		}
	},

	globalUnhandledRejectionListener: function (e) {
		//noinspection JSUnusedAssignment
		var preventDefault = true;
		//<debug>
		preventDefault = false;
		//</debug>
		if (preventDefault)
			e.preventDefault();

		var err = e.detail.reason;
		this._log.fatalException({
			errorMsg: err.message
		}, err);
	}
});