/**
 * Makes a class loggable.
 */
Ext.define('Mext.log.Loggable', {
	extend: 'Ext.Mixin',

	requires: [
		'Mext.log.Log'
	],

	mixinConfig: {
		id: 'mext-loggable',
		before: {
			constructor: 'mextLoggableBeforeConstruction'
		},
		after: {
			destroy: 'mextLoggableAfterDestroy'
		}
	},

	/**
	 * Auto log methods flag.
	 * @protected
	 */
	autoLogMethods: false,

	mextLoggableBeforeConstruction: function () {
		var proto = this.__proto__;
		this._mextLoggableLog = Ext.create('Mext.log.Log', {
			name: this.$className
		});
		if (!this.autoLogMethods) return;
		for (var p in proto) {
			if (!proto.hasOwnProperty(p)) continue;
			if (!Ext.isFunction(proto[p]) || !proto[p].$owner || proto[p].$owner !== this.self || p === 'destroy') continue;
			this.mextLoggableCreateMethodLogger(this, p);
		}
	},

	mextLoggableCreateMethodLogger: function (obj, methodName) {
		var method = obj[methodName] || Ext.emptyFn;
		return (obj[methodName] = function () {
			this.getLog().trace(methodName + '() started.');
			var startTime = new Date().getTime();
			try {
				return method.apply(this, arguments);
			} finally {
				var endTime = new Date().getTime();
				this.getLog().trace(methodName + '() finished in ' + (endTime - startTime) + ' ms.');
			}
		});
	},

	mextLoggableAfterDestroy: function () {
		if (this._mextLoggableLog) {
			this._mextLoggableLog.destroy();
			this._mextLoggableLog = null;
		}
	},

	/**
	 * Gets the logger for this instance.
	 * @returns {Mext.log.Log} Logger for this instance.
	 * @protected
	 */
	getLog: function () {
		return this._mextLoggableLog;
	},

	/**
	 * Logs operation duration.
	 * @param {String} operationName Operation name.
	 * @param {Function} operation Function that performs the operation.
	 * @param {*} [scope] Operation scope. Defaults to `this`.
	 * @protected
	 */
	logOperation: function (operationName, operation, scope) {
		this.getLog().trace(operationName + ' started.');
		var startTime = new Date().getTime();
		try {
			operation.call(scope || this);
		} finally {
			var endTime = new Date().getTime();
			this.getLog().trace(operationName + ' ended in ' + (endTime - startTime) + ' ms.');
		}
	}
});