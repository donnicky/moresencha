/**
 * Represents a JavaScript logger.
 */
Ext.define('Mext.log.Log', {
	config: {
		/**
		 * Defines if log should report to server.
		 */
		ajax: false,

		/**
		 * Defines server report level. Defaults to `ERROR`.
		 */
		ajaxLevel: JL.getErrorLevel(),

		/**
		 * Defines server report URL.
		 */
		url: '/jsnlog',

		/**
		 * Defines if log should report to console.
		 */
		console: true,

		/**
		 * Defines console report level. Defaults to `TRACE`.
		 */
		consoleLevel: JL.getTraceLevel(),

		/**
		 * Defines general logging level. Defaults to `TRACE`
		 */
		level: JL.getTraceLevel(),

		/**
		 * Log name.
		 */
		name: 'Root'
	},

	constructor: function (instanceConfig) {
		var me = this;

		me.initConfig(instanceConfig);
		me._log = new JL(me.getName());
		me.setOptions();
		return me.callParent();
	},

	destroy: function () {
		var me = this;
		me._log = null;
		me._ajaxAppender = null;
		me._consoleAppender = null;
		me.callParent();
	},

	applyAjax: function (value) {
		var me = this, appenderOptions = {};
		me.checkNotDestroyed();
		if (value) {
			if (!me._ajaxAppender) {
				me._ajaxAppender = JL.createAjaxAppender('ajaxAppender');
				if (Ext.isObject(value)) {
					if (Ext.isFunction(value.sender)) {
						appenderOptions.beforeSend = function (xhr) {
							xhr.send = value.sender;
						}
					}

					me._ajaxAppender.setOptions(appenderOptions);
				}
			}
		} else {
			me._ajaxAppender = null;
		}
		me.setOptions();
		return value;
	},

	applyUrl: function (value) {
		var me = this;
		me.checkNotDestroyed();
		if (me._ajaxAppender) {
			me._ajaxAppender.setOptions({
				url: value,
				level: me.getAjaxLevel()
			});
		}
		return value;
	},

	applyAjaxLevel: function (value) {
		var me = this;
		me.checkNotDestroyed();
		if (me._ajaxAppender) {
			me._ajaxAppender.setOptions({
				url: me.getUrl(),
				level: value
			});
		}
		return value;
	},

	applyConsole: function (value) {
		var me = this;
		me.checkNotDestroyed();
		if (value) {
			if (!me._consoleAppender) {
				me._consoleAppender = JL.createConsoleAppender('consoleAppender');
			}
		} else {
			me._consoleAppender = null;
		}
		me.setOptions();
		return value;
	},

	applyConsoleLevel: function (value) {
		var me = this;
		me.checkNotDestroyed();
		if (me._consoleAppender) {
			me._consoleAppender.setOptions({
				level: value
			});
		}
	},

	applyLevel: function (value) {
		this.checkNotDestroyed();
		this.setOptions({level: value});
		return value;
	},

	/**
	 * Creates a log item.
	 * @param {Integer} level Severity of the message to be logged.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	log: function (level, logObject) {
		this.checkNotDestroyed();
		this._log.log(level, logObject);
		return this;
	},

	/**
	 * Creates a log item with severity DEBUG.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	debug: function (logObject) {
		this.checkNotDestroyed();
		this._log.debug(logObject);
		return this;
	},

	/**
	 * Creates a log item with severity ERROR.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	error: function (logObject) {
		this.checkNotDestroyed();
		this._log.error(logObject);
		return this;
	},
	/**
	 * Creates a log item with severity FATAL.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	fatal: function (logObject) {
		this.checkNotDestroyed();
		this._log.fatal(logObject);
		return this;
	},
	/**
	 * Creates a log item with severity FATAL containing a message and an exception.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @param {*} exception Exception that will be logged along with the logObject. On Chrome, Firefox and IE10 and higher, a stack trace will be logged as well.
	 * @returns {Mext.log.Log}
	 */
	fatalException: function (logObject, exception) {
		this.checkNotDestroyed();
		this._log.fatalException(logObject, exception);
		return this;
	},

	/**
	 * Creates a log item with severity INFO.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	info: function (logObject) {
		this.checkNotDestroyed();
		this._log.info(logObject);
		return this;
	},

	/**
	 * Creates a log item with severity TRACE.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	trace: function (logObject) {
		this.checkNotDestroyed();
		this._log.trace(logObject);
		return this;
	},

	/**
	 * Creates a log item with severity WARN.
	 * @param {String|Object|Function} logObject String or object to be logged, or a function that returns the string or object to be logged.
	 * @returns {Mext.log.Log}
	 */
	warn: function (logObject) {
		this.checkNotDestroyed();
		this._log.warn(logObject);
		return this;
	},

	privates: {
		setOptions: function (options) {
			if (!this._log) return;
			options = options || {};
			var me = this,
				appenders = [],
				level = options.level || me.getLevel();

			if (me._ajaxAppender) appenders.push(me._ajaxAppender);
			if (me._consoleAppender) appenders.push(me._consoleAppender);
			me._log.setOptions({
				level: level,
				appenders: appenders
			});
		},

		checkNotDestroyed: function () {
			if (this.destroyed) throw new Mext.error.DestroyedObjectAccessed('Attempt to access destroyed log instance.');
		}
	}
}, function () {
	Mext.error = Mext.error || {};

	Mext.error.DestroyedObjectAccessed = function () {
		var err = Error.apply(this, arguments);
		this.parent.stack = err.stack;
		this.parent.message = err.message || 'Attempt to access a destroyed object.';
		this.parent.name = 'DestroyedObjectAccessed';
		return this;
	};
	Mext.error.DestroyedObjectAccessed.prototype = Object.create(Error.prototype, {
		constructor: {value: Mext.error.DestroyedObjectAccessed},
		parent: {value: Error.prototype}
	});
});