node ./node_modules/cldr-data-downloader/bin/download.js -i bower_components/cldr-data/index.json -o resources/cldr-data/

node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/likelySubtags.json -o ../../../src/localization/cldr/supplemental/LikelySubtags.js -c Mext.localization.cldr.supplemental.LikelySubtags
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/numberingSystems.json -o ../../../src/localization/cldr/supplemental/NumberingSystems.js -c Mext.localization.cldr.supplemental.NumberingSystems
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/ordinals.json -o ../../../src/localization/cldr/supplemental/Ordinals.js -c Mext.localization.cldr.supplemental.Ordinals
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/plurals.json -o ../../../src/localization/cldr/supplemental/Plurals.js -c Mext.localization.cldr.supplemental.Plurals
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/currencyData.json -o ../../../src/localization/cldr/supplemental/CurrencyData.js -c Mext.localization.cldr.supplemental.CurrencyData
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/timeData.json -o ../../../src/localization/cldr/supplemental/TimeData.js -c Mext.localization.cldr.supplemental.TimeData
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/supplemental/weekData.json -o ../../../src/localization/cldr/supplemental/WeekData.js -c Mext.localization.cldr.supplemental.WeekData

node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/root/numbers.json -o ../../../src/localization/cldr/main/root/Numbers.js -c Mext.localization.cldr.main.root.Numbers
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/root/currencies.json -o ../../../src/localization/cldr/main/root/Currencies.js -c Mext.localization.cldr.main.root.Currencies
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/root/ca-gregorian.json -o ../../../src/localization/cldr/main/root/CaGregorian.js -c Mext.localization.cldr.main.root.CaGregorian
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/root/timeZoneNames.json -o ../../../src/localization/cldr/main/root/TimeZoneNames.js -c Mext.localization.cldr.main.root.TimeZoneNames
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/root/dateFields.json -o ../../../src/localization/cldr/main/root/DateFields.js -c Mext.localization.cldr.main.root.DateFields
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/root/languages.json -o ../../../src/localization/cldr/main/root/Languages.js -c Mext.localization.cldr.main.root.Languages
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/en/numbers.json -o ../../../src/localization/cldr/main/en/Numbers.js -c Mext.localization.cldr.main.en.Numbers
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/en/currencies.json -o ../../../src/localization/cldr/main/en/Currencies.js -c Mext.localization.cldr.main.en.Currencies
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/en/ca-gregorian.json -o ../../../src/localization/cldr/main/en/CaGregorian.js -c Mext.localization.cldr.main.en.CaGregorian
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/en/timeZoneNames.json -o ../../../src/localization/cldr/main/en/TimeZoneNames.js -c Mext.localization.cldr.main.en.TimeZoneNames
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/en/dateFields.json -o ../../../src/localization/cldr/main/en/DateFields.js -c Mext.localization.cldr.main.en.DateFields
node ./node_modules/cldr-data-extjs-classifier/bin/classify.js -i ../../../resources/cldr-data/main/en/languages.json -o ../../../src/localization/cldr/main/en/Languages.js -c Mext.localization.cldr.main.en.Languages

rmdir /S /Q resources\cldr-data