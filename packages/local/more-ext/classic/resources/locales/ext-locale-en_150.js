Ext.onReady(function () {
	Ext.Date.defaultFormat = 'd.m.Y';

	if (Ext.util && Ext.util.Format) {
		Ext.apply(Ext.util.Format, {
			thousandSeparator: '.',
			decimalSeparator: ',',
			currencySign: '\u20ac',
			dateFormat: 'd.m.Y'
		});
	}
});

Ext.define("Ext.locale.en150.picker.Date", {
	override: "Ext.picker.Date",
	format: "d.m.Y",
	startDay: 1
});

Ext.define("Ext.locale.150.form.field.Date", {
	override: "Ext.form.field.Date",
	format: "d.m.Y",
	altFormats: "d.m.Y|d-m-y|d-m-Y|d/,|d-m|dm|dmy|dmY|d|Y-m-d",
	startDay: 1
});

Ext.define("Ext.locale.en150.grid.PropertyColumnModel", {
	override: "Ext.grid.PropertyColumnModel",
	dateFormat: "d.m.Y"
});

Ext.define("Ext.locale.en150.grid.NumberColumn", {
	override: "Ext.grid.NumberColumn",
	format: '0,000.##'
});

Ext.define("Ext.locale.en150.grid.DateColumn", {
	override: "Ext.grid.DateColumn",
	format: 'd.m.Y'
});

Ext.define("Ext.locale.en150.form.field.Time", {
	override: "Ext.form.field.Time",
	format: "H:i"
});

Ext.define("Ext.locale.en150.Component", {
	override: "Ext.Component"
});