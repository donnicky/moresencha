Ext.onReady(function () {
	Ext.Date.defaultFormat = 'd.m.Y';
});

Ext.define("Ext.locale.nb.grid.NumberColumn", {
	override: "Ext.grid.NumberColumn",
	format: '0,000.##'
});

Ext.define("Ext.locale.nb.grid.DateColumn", {
	override: "Ext.grid.DateColumn",
	format: 'd.m.Y'
});

Ext.define("Ext.locale.nb.grid.filters.filter.Date", {
	override: "Ext.grid.filters.filter.Date",
	config: {
		fields: {
			lt: {text: 'Før'},
			gt: {text: 'Etter'},
			eq: {text: 'På'}
		}
	}
});

Ext.define('Ext.locale.nb.grid.filters.filter.Number', {
	override: 'Ext.grid.filters.filter.Number',
	emptyText: 'Angi tall...'
});

Ext.define('Ext.locale.nb.grid.filters.filter.String', {
	override: 'Ext.grid.filters.filter.String',
	emptyText: 'Tast filter tekst...'
});

Ext.define("Ext.locale.nb.grid.filters.Filters", {
	override: "Ext.grid.filters.Filters",
	menuFilterText: 'Filtre'
});


Ext.define('Ext.locale.nb.grid.column.Boolean', {
	override: 'Ext.grid.column.Boolean',
	config: {
		trueText: 'sant',
		falseText: 'false'
	}
});

Ext.define("Ext.locale.nb.Component", {
	override: "Ext.Component"
});