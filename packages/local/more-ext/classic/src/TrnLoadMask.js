/**
 * Transparent load mask.
 */
Ext.define('Mext.TrnLoadMask', {
	extend: 'Ext.LoadMask',

	config: {
		/**
		 * Custom message css class.
		 */
		customMsgCls: 'mext-trnloadmask-msg',

		/**
		 * Custom message text css class.
		 */
		customMsgTextCls: 'mext-trnloadmask-msg-text'
	},

	cls: 'mext-trnloadmask',
	msg: '&nbsp',

	initComponent: function () {
		var me = this;

		me.renderTpl = [
			'<div id="{id}-msgWrapEl" data-ref="msgWrapEl" class="' + me.getCustomMsgCls() + '">',
			'<div id="{id}-msgEl" data-ref="msgEl" class="{[values.$comp.msgCls]} ',
			Ext.baseCSSPrefix, 'mask-msg-inner {childElCls}">',
			'<div id="{id}-msgTextEl" data-ref="msgTextEl" class="',
			me.getCustomMsgTextCls(),
			'{childElCls}">{msg}</div>',
			'</div>',
			'</div>'
		];

		me.callParent();
	}
});