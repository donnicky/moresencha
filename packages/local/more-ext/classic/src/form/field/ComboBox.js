//noinspection JSUnusedGlobalSymbols
Ext.define('Mext.form.field.ComboBox', {
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.mext-combobox',

	/**
	 * Finds the record by searching values in the {@link #valueField}.
	 * @param {Object} value The value to match the field against.
	 * @return {Promise} Promise returning the matched record or `false`.
	 */
	findRecordByValue: function (value) {
		var me = this;
		return new Promise(function (resolve) {
			var result = me.store.findRecord(me.valueField, value, 0, false, true, true),
				ret = false,
				isLoadingFromServer = false,
				model;

			// If there are duplicate keys, tested behaviour is to return the *first* match.
			if (result) {
				ret = result[0] || result;
			}

			// Try to load from server
			if (!ret && me.queryMode === 'remote' && value) {
				model = me.store.getModel();
				if (me.valueField === model.idProperty && model.getProxy()) {
					isLoadingFromServer = true;
					model.load(value, {
						callback: function (record, operation, success) {
							if (success && record)
								resolve(record);
							else
								resolve(false);
						}
					});
				}
			}

			if (!isLoadingFromServer) {
				resolve(ret);
			}
		});
	},

	getValue: function () {
		// If the user has not changed the raw field value since a value was selected from the list,
		// then return the structured value from the selection. If the raw field value is different
		// than what would be displayed due to selection, return that raw value.
		var me = this,
			store = me.getStore(),
			picker = me.picker,
			rawValue = me.getRawValue(), //current value of text field
			value = me.value; //stored value from last selection or setValue() call

		// getValue may be called from initValue before a valid store is bound - may still be the default empty one.
		// Also, may be called before the store has been loaded.
		// In these cases, just return the value.
		// In other cases, check that the rawValue matches the selected records.
		//noinspection JSAccessibilityCheck
		if (!store.isEmptyStore && me.getDisplayValue() !== rawValue) {
			me.displayTplData = undefined;
			if (picker) {
				// We do not need to hear about this clearing out of the value collection,
				// so suspend events.
				me.valueCollection.suspendEvents();
				picker.getSelectionModel().deselectAll();
				me.valueCollection.resumeEvents();
				me.lastSelection = null;
			}
			// If the raw input value gets out of sync in a multiple ComboBox, then we have to give up.
			// Multiple is not designed for typing *and* displaying the comma separated result of selection.
			// Same in the case of forceSelection.
			// Unless the store is not yet loaded, which case will be handled in onLoad
			if (store.isLoaded() && (me.multiSelect || me.forceSelection)) {
				// FIX HERE!
				//value = me.value = undefined;
			} else {
				value = me.value = rawValue;
			}
		}

		// Return null if value is undefined/null, not falsy.
		me.value = value == null ? null : value;
		return me.value;
	},

	/**
	 * Gets selected record.
	 * @returns {Promise} Promise returning selected record or false.
	 */
	getSelectedRecord: function () {
		return this.findRecordByValue(this.value);
	},

	/**
	 * @private
	 * Sets or adds a value/values
	 * @returns {Promise}
	 */
	doSetValue: function (value /* private for use by addValue */, add) {
		//noinspection JSAccessibilityCheck
		var me = this,
			store = me.getStore(),
			Model = store.getModel(),
			matchedRecords = [],
			valueArray = [],
			autoLoadOnValue = me.autoLoadOnValue,
			isLoaded = store.getCount() > 0 || store.isLoaded(),
			pendingLoad = store.hasPendingLoad(),
			unloaded = autoLoadOnValue && !isLoaded && !pendingLoad,
			forceSelection = me.forceSelection,
			selModel = me.pickerSelectionModel,
			displayIsValue = me.displayField === me.valueField,
			isEmptyStore = store.isEmptyStore,
			lastSelection = me.lastSelection,
			i, len, record, dataObj,
			valueChanged, key,
			resolver = Promise.pending();

		//<debug>
		//noinspection JSDeprecatedSymbols
		if (add && !me.multiSelect) {
			Ext.raise('Cannot add values to non multiSelect ComboBox');
		}
		//</debug>

		// Called while the Store is loading or we don't have the real store bound yet.
		// Ensure it is processed by the onLoad/bindStore.
		// Even if displayField === valueField, we still MUST kick off a load because even though
		// the value may be correct as the raw value, we must still load the store, and
		// upon load, match the value and select a record sop we can publish the *selection* to
		// a ViewModel.
		if (pendingLoad || unloaded || !isLoaded || isEmptyStore) {

			// If they are setting the value to a record instance, we can
			// just add it to the valueCollection and continue with the setValue.
			// We MUST do this before kicking off the load in case the load is synchronous;
			// this.value must be available to the onLoad handler.
			if (!value.isModel) {
				if (add) {
					me.value = Ext.Array.from(me.value).concat(value);
				} else {
					me.value = value;
				}

				//noinspection JSAccessibilityCheck
				me.setHiddenValue(me.value);

				// If we know that the display value is the same as the value, then show it.
				// A store load is still scheduled so that the matching record can be published.
				me.setRawValue(displayIsValue ? value : '');
			}

			// Kick off a load. Doesn't matter whether proxy is remote - it needs loading
			// so we can select the correct record for the value.
			//
			// Must do this *after* setting the value above in case the store loads synchronously
			// and fires the load event, and therefore calls onLoad inline.
			//
			// If it is still the default empty store, then the real store must be arriving
			// in a tick through binding. bindStore will call setValueOnData.
			if (unloaded && !isEmptyStore) {
				store.load();
			}
		}

		// This method processes multi-values, so ensure value is an array.
		value = add ? Ext.Array.from(me.value).concat(value) : Ext.Array.from(value);

		// Loop through values, matching each from the Store, and collecting matched records
		len = value.length;
		(function step(i) {
			if (i === len) {
				afterLoop();
				return;
			}

			record = value[i];

			// Set value was a key, look up in the store by that key
			if (!record || !record.isModel) {
				me.findRecordByValue(key = record).then(function (foundRecord) {
					if (me.destroyed || me.destroying) {
						resolver.reject('destroyed');
						return;
					}
					record = foundRecord;

					// The value might be in a new record created from an unknown value (if !me.forceSelection).
					// Or it could be a picked record which is filtered out of the main store.
					// Or it could be a setValue(record) passed to an empty store with autoLoadOnValue and aded above.
					if (!record) {
						record = me.valueCollection.find(me.valueField, key);
					}

					afterRecordFound();
				});
			} else
				afterRecordFound();

			function afterRecordFound() {
				// record was not found, this could happen because
				// store is not loaded or they set a value not in the store
				if (!record) {
					// If we are allowing insertion of values not represented in the Store, then push the value and
					// create a new record to push as a display value for use by the displayTpl
					if (!forceSelection) {

						// We are allowing added values to create their own records.
						// Only if the value is not empty.
						if (!record && value[i]) {
							dataObj = {};
							dataObj[me.displayField] = value[i];
							if (me.valueField && me.displayField !== me.valueField) {
								dataObj[me.valueField] = value[i];
							}
							record = new Model(dataObj);
						}
					}
					// Else, if valueNotFoundText is defined, display it, otherwise display nothing for this value
					else if (me.valueNotFoundRecord) {
						record = me.valueNotFoundRecord;
					}
				}
				// record found, select it.
				if (record) {
					matchedRecords.push(record);
					valueArray.push(record.get(me.valueField));
				}

				step(i + 1);
			}
		})(0);

		function afterLoop() {
			// If the same set of records are selected, this setValue has been a no-op
			if (lastSelection) {
				len = lastSelection.length;
				if (len === matchedRecords.length) {
					for (i = 0; !valueChanged && i < len; i++) {
						if (Ext.Array.indexOf(me.lastSelection, matchedRecords[i]) === -1) {
							valueChanged = true;
						}
					}
				} else {
					valueChanged = true;
				}
			} else {
				valueChanged = matchedRecords.length;
			}

			if (valueChanged) {
				// beginUpdate which means we only want to notify this.onValueCollectionEndUpdate after it's all changed.
				me.suspendEvent('select');
				me.valueCollection.beginUpdate();
				if (matchedRecords.length) {
					selModel.select(matchedRecords, false);
				} else {
					selModel.deselectAll();
				}
				me.valueCollection.endUpdate();
				me.resumeEvent('select');
			} else {
				//noinspection JSAccessibilityCheck
				me.updateValue();
			}

			resolver.resolve();
		}

		return resolver.promise;
	},

	checkValueOnChange: function () {
	}
});