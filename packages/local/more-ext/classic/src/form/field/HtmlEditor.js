//noinspection JSUnusedGlobalSymbols
/**
 * @class Mext.form.field.HtmlEditor
 */
Ext.define('Mext.form.field.HtmlEditor', {
	extend: 'Ext.form.field.HtmlEditor',
	alias: 'widget.mext-htmleditor',

	config: {
		/**
		 * @cfg {Boolean} [openLinksInNewTab=false] If `true` then links are opened in new tabs.
		 */
		openLinksInNewTab: false,

		/**
		 * @cfg {Boolean} [enterToSubmit=false] If `true` then Enter does not create a new paragraph but fires `submit` event. New paragraph is created by Ctrl+Enter.
		 */
		enterToSubmit: false
	},

	/**
	 * @private
	 * Creates a link that can be opened in new tab.
	 */
	createLink: function () {
		if (!this.getOpenLinksInNewTab()) return this.callParent();
		var url = prompt(this.createLinkText, this.defaultLinkValue),
			selection = this.getDoc().getSelection();
		if (url && url !== 'http:/' + '/') {
			this.relayCmd('insertHTML', '<a href="' + url + '" target="_blank">' + (selection.toString() ? selection : url) + '</a>');
		}
	},

	/**
	 * @private
	 * Initialized the editor considering {@link #enterToSubmit} option.
	 */
	initEditor: function () {
		var me = this, doc, docEl;

		me.callParent();

		if (me.destroying || me.isDestroyed || !me.getEnterToSubmit()) return;

		doc = me.getDoc();
		docEl = Ext.get(doc);

		docEl.dom.addEventListener('keydown', function (e) {
			if (e.keyCode === 13) {
				if (!e.ctrlKey) {
					e.preventDefault();
					me.fireEvent('submit');
				} else {
					doc.execCommand('insertParagraph', false);
				}
			}
		});
	}
});