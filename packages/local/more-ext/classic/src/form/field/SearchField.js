/**
 * @class Mext.form.field.SearchField
 */
Ext.define('Mext.form.field.SearchField', {
	extend: 'Ext.form.field.Text',

	alias: 'widget.mext-searchfield',

	triggers: {
		clear: {
			weight: 0,
			cls: Ext.baseCSSPrefix + 'form-clear-trigger',
			hidden: true,
			handler: 'onClearClick',
			scope: 'this'
		},
		search: {
			weight: 1,
			cls: Ext.baseCSSPrefix + 'form-search-trigger',
			handler: 'onSearchClick',
			scope: 'this'
		}
	},

	config: {
		/**
		 * @cfg {Ext.data.Store} store (required) Store to filter.
		 */
		store: null,

		/**
		 * @cfg {String} filterProperty (required) Property to apply filter to.
		 */
		filterProperty: null,

		/**
		 * @cfg {String} [filterOperator="like"] Filtering operator.
		 */
		filterOperator: 'like'
	},

	applyStore: function (store) {
		if (store && !store.isStore)
			return Ext.getStore(store);
		return store;
	},

	initComponent: function () {
		var me = this;

		me.callParent(arguments);
		me.on('specialkey', function (f, e) {
			if (e.getKey() == e.ENTER) {
				me.onSearchClick();
			}
		});

		me.on('blur', me.onSearchClick, me);
	},

	onClearClick: function () {
		var me = this,
			activeFilter = me.activeFilter;

		if (activeFilter) {
			me.setValue('');
			me.getStore().getFilters().remove(activeFilter);
			me.activeFilter = null;
			me.getTrigger('clear').hide();
			me.updateLayout();
		}
	},

	onSearchClick: function () {
		var me = this,
			value = me.getValue(),
			filters = me.getStore().getFilters();

		if (value.length > 0) {
			me.activeFilter = filters.add({
				id: 'mextSearchFieldFilter',
				property: me.getFilterProperty(),
				value: value,
				operator: me.getFilterOperator()
			});
			me.getTrigger('clear').show();
			me.updateLayout();
		} else {
			me.onClearClick();
		}
	}
});