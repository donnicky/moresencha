Ext.define('Mext.SyncIndicator', {
	extend: 'Ext.Container',
	mixins: [
		'Mext.localization.Localizable'
	],
	xtype: 'mext-syncindicator',

	requires: [
		'Ext.layout.container.HBox',
		'Mext.IconsManager',
		'Mext.locale.Default'
	],

	config: {
		/**
		 * Message to show during sync. If not set then default localized message is used.
		 */
		syncMessage: null
	},

	layout: 'hbox',
	initComponent: function() {
		var me = this;

		me.items = [
			{
				xtype: 'component',
				cls: Mext.IconsManager.LOADING_INDICATOR.small,
				width: 16,
				height: 16,
				style: 'margin-right: 3px;'
			},
			{
				xtype: 'component',
				html: me.L('defaultSyncMessage')
			}
		];

		me.callParent();
	}
});