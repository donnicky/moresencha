/**
 * Login dialog.
 */
Ext.define('Mext.dialog.Login', {
	extend: 'Ext.window.Window',
	alias: 'widget.mext-logindlg',
	mixins: [
		'Mext.localization.Localizable'
	],

	requires: [
		'Ext.Ajax',
		'Ext.Error',
		'Ext.JSON',
		'Ext.button.Button',
		'Ext.form.FieldContainer',
		'Ext.form.Panel',
		'Ext.form.field.Display',
		'Ext.layout.container.Fit',
		'Ext.layout.container.Form',
		'Ext.layout.container.VBox',
		'Ext.menu.CheckItem',
		'Ext.toolbar.Fill',
		'Mext.IconsManager',
		'Mext.locale.Default',
		'Mext.localization.Localization'
	],

	config: {
		/**
		 * Login POST action URL.
		 */
		actionUrl: '/auth',

		/**
		 * Auto status GET action url.
		 */
		authStatusUrl: '/authStatus',

		/**
		 * Locales defined in system to show locale selection menu. For example: `['en', 'ru']`.
		 * If an array is empty then tries to get locales from {@link Mext.localization.Localization#getAvailableLocales}.
		 * If still empty then menu is not displayed.
		 */
		locales: [],

		/**
		 * Additional toolbar items to show after language selector.
		 */
		moreToolbarItems: [],

		/**
		 * Toolbar items padding.
		 */
		toolbarItemsPadding: 15
	},

	/**
	 * @event localesload
	 * @param {Array} localesInfo As return by {@link Mext.localization.Localization#getLocalesInfo}.
	 */

	/**
	 * @protected
	 */
	initConfig: function (instanceConfig) {
		var me = this,
			L = this.L.bind(this),
			config;

		config = {
			title: L('defaultTitle'),
			layout: 'fit'
		};

		Ext.apply(config, instanceConfig || {});

		me.callParent([config]);
	},

	initComponent: function () {
		var me = this,
			L = me.L.bind(me),
			showLocaleSelector = Ext.isArray(me.getLocales()) && me.getLocales().length > 0,
			moreToolbarItems = me.getMoreToolbarItems(),
			toolbarItemsPadding = me.getToolbarItemsPadding(),
			toolbarItems = [],
			formConfig;

		me.items = [
			formConfig = {
				xtype: 'form',
				standardSubmit: true,
				layout: {
					type: 'vbox',
					align: 'stretch'
				},
				autoEl: {
					tag: 'form',
					method: 'post',
					action: me.getActionUrl(),
					autocomplete: 'on'
				},
				items: [
					{
						xtype: 'fieldcontainer',
						layout: 'form',
						defaultType: 'textfield',
						items: [
							{
								fieldLabel: L('username'),
								inputAttrTpl: 'autocomplete="on"',
								name: 'username'
							},
							{
								fieldLabel: L('password'),
								inputAttrTpl: 'autocomplete="on"',
								name: 'password',
								inputType: 'password'
							}
						]
					},
					{
						xtype: 'displayfield',
						fieldStyle: 'text-align: center; color: red; font-weight: bold',
						hidden: true,
						style: {
							paddingLeft: '5px',
							paddingRight: '5px'
						},
						listeners: {
							afterrender: function (field) {
								Ext.Ajax.request({
									url: me.getAuthStatusUrl(),
									success: function (response) {
										var data = Ext.JSON.decode(response.responseText, true);
										var value;
										if (Ext.isArray(data)) {
											if (data.length > 0) value = data.join('<br/>');
										} else if (Ext.isString(data)) {
											value = data;
										}

										if (value) {
											//<debug>
											console.debug(value);
											//</debug>
											field.setHidden(false);
											field.setValue(value);
										}
									}
								});
							}
						}
					}
				]
			}
		];

		me.localeSelector = Ext.widget('component', {
			cls: Mext.IconsManager.LOADING_INDICATOR.small,
			itemId: 'localeSelector',
			height: 16,
			style: 'padding-left: 20px',
			html: L('loadingLocales'),
			hidden: !showLocaleSelector
		});

		toolbarItems.push(me.localeSelector);
		toolbarItems = toolbarItems.concat(moreToolbarItems);
		toolbarItems = toolbarItems.concat([
			{
				xtype: 'tbfill'
			},
			{
				xtype: 'button',
				autoEl: {
					tag: 'input',
					type: 'submit',
					value: L('action')
				},
				preventDefault: false
			}
		]);

		//<debug>
		console.debug(moreToolbarItems);
		console.debug(toolbarItems);
		//</debug>

		formConfig.dockedItems = [
			me.toolbar = Ext.widget('toolbar', {
				dock: 'bottom',
				ui: 'footer',
				defaults: {
					style: 'padding-left: {1}px;padding-right: {1}px;'.assign(toolbarItemsPadding)
				},
				items: toolbarItems
			})
		];

		me.toolbar.on('afterlayout', me.ensureFullToolbarVisible, me, {single: true});

		me.callParent();

		if (showLocaleSelector) {
			Mext.localization.Localization.getLocalesInfo(me.getLocales()).then(function (localesDisplayNames) {
				me.toolbar.remove(me.localeSelector, true);
				me.localeSelector = me.toolbar.insert(0, {
					xtype: 'button',
					itemId: 'localeSelector',
					text: Mext.localization.Localization.getCurrentLocaleDisplayName(),
					iconCls: Mext.localization.Localization.getCurrentLocaleFlagCssStyle(),
					menu: me.createLocaleSelectionMenu(localesDisplayNames)
				});
				me.fireEvent('localesload', me, localesDisplayNames);
				me.ensureFullToolbarVisible();
			}).catch(function (err) {
				//<debug>
				throw err;
				//</debug>
			});
		}
	},

	applyLocales: function (value) {
		if (!Ext.isArray(value) || value.length === 0)
			value = Mext.localization.Localization.getAvailableLocales();
		return value;
	},

	applyMoreToolbarItems: function (value) {
		//<debug>
		console.debug(value);
		//</debug>

		if (!Ext.isArray(value))
			value = [];
		return value;
	},

	/**
	 * @private
	 */
	ensureFullToolbarVisible: function () {
		var me = this,
			tb = me.toolbar,
			tbLayout = tb.getLayout(),
			toolbarItemsPadding = me.getToolbarItemsPadding(),
			overflowWidth = tbLayout.targetEl.el.dom.scrollWidth - tbLayout.targetEl.el.dom.offsetWidth + toolbarItemsPadding;
		if (overflowWidth > 0)
			me.setWidth(me.getWidth() + overflowWidth);
	},

	/**
	 * @private
	 */
	createLocaleSelectionMenu: function (localesDisplayNames) {
		var me = this,
			items = [];

		localesDisplayNames.forEach(function (x) {
			items.push({
				xtype: 'menucheckitem',
				text: x.displayName,
				group: 'locale',
				iconCls: Mext.localization.Localization.getLocaleFlagCssStyle(x.territory),
				checked: x.locale === Mext.localization.Localization.getDefaultLocale(),
				handler: function () {
					if (this.text === me.localeSelector.getText()) return;
					me.localeSelector.setText(x.displayName);
					me.localeSelector.setIconCls(Mext.localization.Localization.getLocaleFlagCssStyle(x.territory));
					me.setLoading(me.L('applying'));
					Mext.localization.Localization.setDefaultLocale(x.locale).then(function () {
						me.setLoading(false);
						window.location.reload();
					});
				}
			});
		});

		var menu = Ext.widget('menu', {
			items: items
		});

		return menu;
	}
});