Ext.define('Mext.locale.Default', {
	requires: [
		'Mext.helpers.ResourcesManager',
		'Mext.localization.Localization'
	],
	singleton: true,
	data: {
		"root": {
			"Mext.dialog.Login": {
				"defaultTitle": "Log in",
				"username": "Username",
				"password": "Password",
				"action": "Log in",
				"unknownLocale": "Unknown",
				"loadingLocales": "Loading...",
				"applying": "Applying..."
			},
			"Mext.grid.column.Association": {
				"loadingCell": "loading...",
				"loadingCellError": "error!",
				"notFound": "not found!"
			},
			"Mext.SyncIndicator": {
				"defaultSyncMessage": "Saving..."
			}
		}
	}
}, function () {
	Globalize.loadMessages(this.data);
	Mext.localization.Localization.addLocaleData(Mext.helpers.ResourcesManager.ensurePath().then(function (p) {
		return p + '/more-ext/locales/{1}.json'
	}));
});