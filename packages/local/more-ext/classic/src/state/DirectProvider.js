Ext.define('Mext.state.DirectProvider', {
	extend: 'Ext.state.Provider',

	config: {
		directGetStateFn: Ext.emptyFn,
		directSetStateFn: Ext.emptyFn,
		directClearStateFn: Ext.emptyFn
	},

	constructor: function (instanceConfig) {
		var me = this;
		me.initConfig(instanceConfig);
		me.callParent(arguments);
	},

	getState: function (callback) {
		var me = this,
			state = {};

		me.directGetStateFn(function (data, response, success) {
			if (typeof callback === 'function') {
				if (!success) {
					callback(new Error(response.message));
					return;
				}
				for (var stateKey in data) {
					if (data.hasOwnProperty(stateKey))
						state[stateKey] = me.decodeValue(data[stateKey]);
				}
				me.state = state;
				callback(null);
			}
		});
	},

	set: function (name, value) {
		var me = this;

		//<debug>
		console.debug([name, value]);
		//</debug>
		me.directSetStateFn({key: name, value: me.encodeValue(value)});

		me.callParent(arguments);
	},

	clear: function (name) {
		var me = this;

		me.directClearStateFn(name);

		me.callParent(arguments);
	}
});