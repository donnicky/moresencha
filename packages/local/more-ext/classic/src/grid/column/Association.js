/**
 * Column for handling associated fields.
 */
Ext.define('Mext.grid.column.Association', {
	extend: 'Ext.grid.column.Column',
	mixins: [
		'Mext.localization.Localizable'
	],
	alias: 'widget.mext-associationcolumn',

	requires: [
		'Mext.locale.Default'
	],

	config: {
		/**
		 * Field of the associated record to show in cell.
		 */
		displayField: 'text',

		/**
		 * @cfg {Function} valueRenderer Custom function to render value.
		 */
		valueRenderer: null,

		/**
		 * @cfg {Function} errorRenderer Custom function to render error.
		 */
		errorRenderer: null,

		/**
		 * @cfg {Function} notFoundRenderer Custom function to render message when associated record has not been found.
		 */
		notFoundRenderer: null,

		/**
		 * @cfg {Ext.data.Store} store Store to find associated record. If set then used instead of association getter.
		 */
		store: null
	},

	defaultRenderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
		if (record.dropped) return '';
		if (value == null) return this.emptyValueText;
		var me = this,
			getter = me._associatedValueGetter,
			vrStore = me.getStore(),
			valueRecord, isAsync = false;

		if (vrStore) {
			valueRecord = vrStore.getById(value);
		}
		else {
			if (!getter) {
				for (var assoc in record.associations) {
					if (!record.associations.hasOwnProperty(assoc)) continue;
					if (record.associations[assoc].association.field.name !== me.dataIndex) continue;
					me._associatedValueGetter = getter = record.associations[assoc].getterName;
				}
			}
			record[getter]({
				success: function (loadedRecord) {
					valueRecord = loadedRecord;
					if (isAsync && !record.dropped) {
						var row = view.getRow(record),
							rowEl = Ext.fly(row),
							cellInnerEl = rowEl ? rowEl.down(me.getCellInnerSelector()) : null;

						if (cellInnerEl) {
							cellInnerEl.setHtml(me.renderValue(valueRecord, metaData, record, rowIndex, colIndex, store, view));
						}
					}
				},
				failure: function (failedRecord, operation) {
					if (isAsync && !record.dropped) {
						var row = view.getRow(record),
							rowEl = Ext.fly(row),
							cellInnerEl = rowEl ? rowEl.down(me.getCellInnerSelector()) : null;

						if (cellInnerEl) {
							if (!operation.success)
								cellInnerEl.setHtml(me.renderError(me.L('loadingCellError'), metaData, record, rowIndex, colIndex, store, view));
							else
								cellInnerEl.setHtml(me.renderNotFound(me.L('notFound'), metaData, record, rowIndex, colIndex, store, view));
						}
					}
				}
			});
		}

		isAsync = true;
		if (valueRecord)
			return me.renderValue(valueRecord, metaData, record, rowIndex, colIndex, store, view);

		return me.L('loadingCell');
	},

	privates: {
		renderValue: function (valueRecord, metaData, record, rowIndex, colIndex, store, view) {
			var renderer = this.getValueRenderer();
			if (Ext.isFunction(renderer))
				return renderer.call(this, valueRecord, metaData, record, rowIndex, colIndex, store, view);
			return valueRecord.get(this.getDisplayField());
		},
		renderError: function (value, metaData, record, rowIndex, colIndex, store, view) {
			var renderer = this.getErrorRenderer();
			if (Ext.isFunction(renderer))
				return renderer.call(this, value, metaData, record, rowIndex, colIndex, store, view);
			return '<span style="color:red">' + value + '</span>';
		},
		renderNotFound: function (value, metaData, record, rowIndex, colIndex, store, view) {
			var renderer = this.getNotFoundRenderer();
			if (Ext.isFunction(renderer))
				return renderer.call(this, value, metaData, record, rowIndex, colIndex, store, view);
			return '<span style="color:magenta">' + value + '</span>';
		}
	}
});