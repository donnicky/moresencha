Ext.define('Mext.override.form.field.Picker', {
	override: 'Ext.form.field.Picker',
	compatibility: '6.0.1.250',

	initComponent: function () {
		this.callParent(arguments);

		// EXTJS-18574. https://www.sencha.com/forum/showthread.php?303101
		this.pickerId = this.getId() + "_Picker";
	}
});