Ext.define('Mext.override.form.field.ComboBox', {
	override: 'Ext.form.field.ComboBox',

	config: {
		clearTrigger: false
	},

	triggers: {
		clear: {
			weight: 0,
			cls: Ext.baseCSSPrefix + 'form-clear-trigger',
			hidden: true,
			handler: function (combo) {
				if (combo.fireEvent('beforeclear', combo) !== false) {
					combo.clearValue();
					combo.fireEvent('clear', combo, null);
				}
			}
		}
	},

	initComponent: function () {
		var me = this;

		if (me.getClearTrigger()) {
			me.on('change', function (combo, newValue) {
				me.showOrHideClearTrigger(newValue);
			});

			me.on('select', function () {
				me.showOrHideClearTrigger(true);
			});

			me.on('focus', function () {
				me.showOrHideClearTrigger();
			});

			me.on('render', function () {
				me.showOrHideClearTrigger();
			});

			me.on('blur', function () {
				me.showOrHideClearTrigger();
			});

			me.on('beforeshow', function () {
				me.showOrHideClearTrigger();
			});
		}

		me.callParent(arguments);
	},

	showOrHideClearTrigger: function (value) {
		if (!this.getClearTrigger()) return;
		var t = this.getTrigger('clear');
		if (!t) return;
		if (typeof value === 'undefined')
			value = this.getValue();
		if (!value || Ext.isArray(value) && value.length === 0 || this.readOnly)
			t.hide();
		else
			t.show();
	}
});