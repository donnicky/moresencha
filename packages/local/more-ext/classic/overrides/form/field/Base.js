/**
 * Created by Nikolay on 30.08.2015.
 */
Ext.define('Mext.override.form.field.Base', {
	override: 'Ext.form.field.Base',

	config: {
		tooltip: false
	},

	applyTooltip: function (value) {
		var me = this;

		if (!me.rendered) {
			me.on('afterrender', me.mextMakeTooltip, me, {single: true});
		} else {
			me.mextMakeTooltip({tooltip: value});
		}

		return value;
	},

	mextMakeTooltip: function (opts) {
		opts = opts || {};
		var tooltip = this.getTooltip(),
			el = this.getEl();

		if (opts.hasOwnProperty('tooltip'))
			tooltip = opts.tooltip;

		if (!this._mextTooltipRegistered && !tooltip) return;

		Ext.QuickTips.unregister(el);

		if (tooltip) {
			Ext.QuickTips.register({
				target: el,
				text: tooltip
			});
			this._mextTooltipRegistered = true;
		}
	}
});