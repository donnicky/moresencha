Ext.define('Mext.override.data.schema.Role', {
	override: 'Ext.data.schema.Role',
	statics: {
		targetVersion: '6.0.1.250'
	},

	doGetFK: function (leftRecord, options, scope) {
		// Consider the Department entity with a managerId to a User entity. This method
		// is the guts of the getManager method that we place on the Department entity to
		// acquire a User entity. We are the "manager" role and that role describes a
		// User. This method is called, however, given a Department (leftRecord) as the
		// start of this trek.

		var me = this,    // the "manager" role
			cls = me.cls,  // User
			foreignKey = me.association.getFieldName(),  // "managerId"
			instanceName = me.getInstanceName(),  // "manager"
			rightRecord = leftRecord[instanceName], // = department.manager
			reload = options && options.reload,
			done = rightRecord !== undefined && !reload,
			session = leftRecord.session,
			foreignKeyId, args;

		// FIX HERE!
		if (done && foreignKey && rightRecord) {
			foreignKeyId = leftRecord.get(foreignKey);
			if (foreignKeyId !== rightRecord.getId()) {
				done = false;
				rightRecord = null;
			}
		}

		if (!done) {
			// We don't have the User record yet, so try to get it.

			if (session) {
				foreignKeyId = leftRecord.get(foreignKey);
				if (foreignKeyId || foreignKeyId === 0) {
					done = session.peekRecord(cls, foreignKeyId, true) && !reload;
					rightRecord = session.getRecord(cls, foreignKeyId, false);
				} else {
					done = true;
					leftRecord[instanceName] = rightRecord = null;
				}
			} else if (foreignKey) {
				// The good news is that we do indeed have a FK so we can do a load using
				// the value of the FK.

				foreignKeyId = leftRecord.get(foreignKey);
				if (!foreignKeyId && foreignKeyId !== 0) {
					// A value of null ends that hope though... but we still need to do
					// some callbacks perhaps.
					done = true;
					leftRecord[instanceName] = rightRecord = null;
				} else {
					// foreignKeyId is the managerId from the Department (record), so
					// make a new User, set its idProperty and load the real record via
					// User.load method.
					if (!rightRecord) {
						// We may be reloading, let's check if we have one.
						rightRecord = cls.createWithId(foreignKeyId);
					}
					// we are not done in this case, so don't set "done"
				}
			} else {
				// Without a FK value by which to request the User record, we cannot do
				// anything. Declare victory and get out.
				done = true;
			}
		} else if (rightRecord) {
			// If we're still loading, call load again which will handle the extra callbacks.
			done = !rightRecord.isLoading();
		}

		if (done) {
			if (options) {
				args = [rightRecord, null];
				scope = scope || options.scope || leftRecord;

				Ext.callback(options.success, scope, args);
				args.push(true);
				Ext.callback(options, scope, args);
				Ext.callback(options.callback, scope, args);
			}
		} else {
			leftRecord[instanceName] = rightRecord;
			options = me.getCallbackOptions(options, scope, leftRecord);
			rightRecord.load(options);
		}

		return rightRecord;
	}
}, function () {
	//noinspection JSAccessibilityCheck
	if (!Ext.versions.ext.equals(this.targetVersion)) {
		//noinspection JSAccessibilityCheck
		Ext.log.warn(this.$className + ' override targets ' + this.targetVersion + ' version, but used for ' + Ext.versions.ext.toString() + '!');
	}
});