Ext.define('Mext.override.data.AbstractStore', {
	override: 'Ext.data.AbstractStore',

	getFilterByProperty: function (prop) {
		var me = this,
			filter,
			filters = me.getFilters().getRange();

		filter = filters.find(function (x) {
			return x.getProperty() === prop;
		});

		return filter;
	},

	getFilterByPropertyAndOperator: function (prop, operator) {
		var me = this,
			filter,
			filters = me.getFilters().getRange();

		filter = filters.find(function (x) {
			return x.getProperty() === prop
				&& (x.getOperator() === operator
				|| !x.getOperator() && !operator
				|| x.getOperator() === 'eq' && !operator
				|| !x.getOperator() && operator === 'eq');
		});

		return filter;
	},

	removeFilterByProperty: function (prop, suppressEvent) {
		var filter = this.getFilterByProperty(prop);
		if (filter)
			this.removeFilter(filter, suppressEvent);
	},

	removeFilterByPropertyAndOperator: function (prop, operator, suppressEvent) {
		var filter = this.getFilterByPropertyAndOperator(prop, operator);
		if (filter)
			this.removeFilter(filter, suppressEvent);
	},

	isFilterValueEmpty: function (value) {
		return value == null || Ext.isArray(value) && value.length === 0;
	}
});