//noinspection JSUnusedGlobalSymbols
/**
 * @class Mext.override.data.writer.Json
 * @override Ext.data.writer.Json
 */
Ext.define('Mext.override.data.writer.Json', {
	override: 'Ext.data.writer.Json',
	statics: {
		targetVersion: '6.0.1.250'
	},

	/**
	 * Override that adds extra params to request body if root property is defined.
	 * @param request
	 * @param data
	 * @returns {*}
	 */
	writeRecords: function (request, data) {
		var me = this,
			root = me.getRootProperty(),
			json, single, transform;

		if (me.getExpandData()) {
			data = me.getExpandedData(data);
		}

		if (me.getAllowSingle() && data.length === 1) {
			// convert to single object format
			data = data[0];
			single = true;
		}

		transform = this.getTransform();
		if (transform) {
			data = transform(data, request);
		}

		if (me.getEncode()) {
			if (root) {
				// sending as a param, need to encode
				request.setParam(root, Ext.encode(data));
			} else {
				//<debug>
				Ext.raise('Must specify a root when using encode');
				//</debug>
			}
		} else if (single || (data && data.length)) {
			// send as jsonData
			json = request.getJsonData() || {};
			if (root) {
				// FIX HERE!!! Adds request params to json body.
				if (request.getParams()) Ext.apply(json, request.getParams());

				json[root] = data;
			} else {
				json = data;
			}
			request.setJsonData(json);
		}
		return request;
	}
}, function () {
	//noinspection JSAccessibilityCheck
	if (!Ext.versions.ext.equals(this.targetVersion)) {
		//noinspection JSAccessibilityCheck
		Ext.log.warn(this.$className + ' override targets ' + this.targetVersion + ' version, but used for ' + Ext.versions.ext.toString() + '!');
	}
});