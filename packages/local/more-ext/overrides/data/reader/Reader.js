/**
 * @class Mext.override.data.reader.Reader
 */
Ext.define('Mext.override.data.reader.Reader', {
	override: 'Ext.data.reader.Reader',
	compatibility: '6.0.1.250',

	readAssociated: function (record, data, readOptions) {
		var roles = record.associations,
			key, role;

		for (key in roles) {
			if (roles.hasOwnProperty(key)) {
				role = roles[key];
				// The class for the other role may not have loaded yet
				if (role.cls) {
					//  EXTJS-16282: https://www.sencha.com/forum/showthread.php?295904
					role.read(record, data, this, Ext.apply({}, {model: role.type}, readOptions));
				}
			}
		}
	}
});