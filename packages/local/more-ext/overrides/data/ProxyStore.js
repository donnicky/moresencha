/**
 * @class Mext.override.data.ProxyStore
 */
Ext.define('Mext.override.data.ProxyStore', {
	override: 'Ext.data.ProxyStore',
	statics: {
		targetVersion: '6.0.1.250'
	},

	hasRecordsToSync: function () {
		var me = this,
			toCreate = me.getNewRecords(),
			toUpdate = me.getUpdatedRecords(),
			toDestroy = me.getRemovedRecords();

		return !!(toCreate.length > 0 || toUpdate.length > 0 || toDestroy.length > 0);
	},

	onBatchComplete: function (batch) {
		var me = this,
			operations = batch.getOperations(),
			operationsCount = operations.length, i, j, k,
			nonRequestChangedRecords = [],
			nonRequestNewRecords = [];
		for (i = 0; i < operationsCount; i++) {
			var operation = operations[i];
			if (operation.wasSuccessful()) {
				var reader = operation.getProxy().getReader(),
					rawData = reader.rawData,
					entityType = reader.getModel(),
					idProperty = entityType.idProperty,
					schema = entityType.schema,
					includes = schema.hasAssociations(entityType) && reader.getImplicitIncludes(),
					fieldExtractorInfo = reader.getFieldExtractorInfo(entityType.fieldExtractors),
					resultSetRecords = operation.getResultSet().getRecords(),
					resultSetRecordsCount = resultSetRecords.length,
				// Here we collect changes in records which are not processed by standard store logic.
				// Standard logic processes records which were in the request only.
					recordsToProcess = [],
					recordsToProcessRawData = [],
					recordsProcessedByOperation = operation.getRecords(),
					recordsProcessedByOperationCount = recordsProcessedByOperation.length,
					assocInstanceName;

				// Get records which were not in request and require custom processing.
				for (j = 0; j < resultSetRecordsCount; j++) {
					var processed = false;

					for (k = 0; k < recordsProcessedByOperationCount; k++) {
						if (recordsProcessedByOperation[k].getId() == resultSetRecords[j][idProperty]) {
							processed = true;
							break;
						}
					}

					if (!processed) {
						if (rawData)
							recordsToProcessRawData.push(Ext.clone(rawData[j]));
						recordsToProcess.push(resultSetRecords[j]);
					}
				}

				var recordsToProcessCount = recordsToProcess.length,
					existingRecordsToRefresh = [],
					unprocessedNewRecords = [],
					unprocessedNewRecordsData = [];
				for (j = 0; j < recordsToProcessCount; j++) {
					var recordData = recordsToProcess[j], record;
					if (!recordData.isModel) {
						var extractedRecordData = Ext.clone(recordData);
						record = reader.extractRecord(extractedRecordData, {}, entityType, includes, fieldExtractorInfo);
						record.raw = extractedRecordData;
					}
					else {
						record = recordData;
						if (recordsToProcessRawData.length > 0)
							recordData = recordsToProcessRawData[j];
						else
							recordData = record.raw;
					}
					var existingRecord = me.getById(record.getId());

					// IMPORTANT! Record should have 'isDeleted' field to let logic know how to process it.
					if (existingRecord) {
						//region Process existing records.
						if (record.get('isDeleted')) {
							//region Process deletion.
							me.suspendAutoSync();
							existingRecord.drop(false);
							me.removedNodes.splice(me.removedNodes.indexOf(existingRecord), 1);
							me.resumeAutoSync();
							//endregion
						}
						else {
							//region Update fields of existing record with new data from server.
							// TODO: recordData usage is potentially dangerous: it will break logic if properties of models are mapped to other server-side response fields.
							existingRecord.beginEdit();
							for (var entityProp in recordData) {
								if (!recordData.hasOwnProperty(entityProp) || entityProp === existingRecord.idProperty) continue;
								existingRecord.set(entityProp, recordData[entityProp]);
								//existingRecord.data[entityProp] = recordData[entityProp];
							}

							// https://www.sencha.com/forum/showthread.php?291474
							for (var assocProp in record.associations) {
								//noinspection JSUnresolvedFunction
								if (!record.associations.hasOwnProperty(assocProp)) continue;
								assocInstanceName = record.associations[assocProp].getInstanceName();
								delete existingRecord[assocInstanceName];
								existingRecord[assocInstanceName] = record[assocInstanceName];
							}

							nonRequestChangedRecords.push(existingRecord);
							existingRecordsToRefresh.push(existingRecord);
							//endregion
						}
						//endregion
					}
					else {
						// Collect new unprocessed records.
						if (!record.get('isDeleted')) {
							nonRequestNewRecords.push(record);
							unprocessedNewRecords.push(record);
							unprocessedNewRecordsData.push(recordData);
						}
					}
				}

				existingRecordsToRefresh.forEach(function (x) {
					x.endEdit(true);
					x.commit();
				});
				//me.fireEvent('refresh', me, existingRecordsToRefresh);

				// Processing of new records can differ for different types of stores: Store, TreeStore and so on.
				me.processNewRecordsFromSync(unprocessedNewRecords, unprocessedNewRecordsData);
			}
		}

		me.callParent(arguments);

		me.fireEvent('synccomplete', me, batch, nonRequestChangedRecords, nonRequestNewRecords);
	},

	processNewRecordsFromSync: Ext.emptyFn,

	/**
	 * @private
	 * Filter function for updated records.
	 */
	filterUpdated: function (item) {
		// ADDED !item.dropped
		// only want dirty records, not phantoms that are valid
		return item.dirty === true && !item.dropped && item.phantom !== true && item.isValid();
	}
}, function () {
	//noinspection JSAccessibilityCheck
	if (!Ext.versions.ext.equals(this.targetVersion)) {
		//noinspection JSAccessibilityCheck
		Ext.log.warn(this.$className + ' override targets ' + this.targetVersion + ' version, but used for ' + Ext.versions.ext.toString() + '!');
	}
});