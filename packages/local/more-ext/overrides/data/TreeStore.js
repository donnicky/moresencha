/**
 * @class Mext.override.data.TreeStore
 */
Ext.define('Mext.override.data.TreeStore', {
	override: 'Ext.data.TreeStore',
	compatibility: '6.0.1.250',

	// EXTJS-15457
	sync: function (options) {
		var me = this,
			operations = {},
			toCreate = me.getNewRecords(),
			toUpdate = me.getUpdatedRecords(),
			toDestroy = me.getRemovedRecords(),
			needsSync = false,
			className,
			operationsForClass,
			i;

		if (me.isSyncing) {
			Ext.log.warn('Sync called while a sync operation is in progress. Consider configuring autoSync as false.');
		}

		me.needsSync = false;

		var toCreateLength = toCreate.length;
		if (toCreateLength > 0) {
			for (i = 0; i < toCreateLength; i++) {
				className = toCreate[i].__proto__.$className;
				operationsForClass = operations[className] = operations[className] || {};
				operationsForClass.create = operationsForClass.create || [];
				operationsForClass.create[operationsForClass.create.length] = toCreate[i];
			}

			needsSync = true;
		}

		var toUpdateLength = toUpdate.length;
		if (toUpdateLength > 0) {
			for (i = 0; i < toUpdateLength; i++) {
				className = toUpdate[i].__proto__.$className;
				operationsForClass = operations[className] = operations[className] || {};
				operationsForClass.update = operationsForClass.update || [];
				operationsForClass.update[operationsForClass.update.length] = toUpdate[i];
			}

			needsSync = true;
		}

		var toDestroyLength = toDestroy.length;
		if (toDestroyLength > 0) {
			for (i = 0; i < toDestroyLength; i++) {
				className = toDestroy[i].__proto__.$className;
				operationsForClass = operations[className] = operations[className] || {};
				operationsForClass.destroy = operationsForClass.destroy || [];
				operationsForClass.destroy[operationsForClass.destroy.length] = toDestroy[i];

			}

			needsSync = true;
		}

		if (needsSync && me.fireEvent('beforesync', operations) !== false) {
			me.isSyncing = true;

			options = options || {};

			for (className in operations) {
				if (!operations.hasOwnProperty(className)) continue;
				var classOperations = operations[className];
				//noinspection JSCheckFunctionSignatures
				Ext.ClassManager.get(className).getProxy().batch(Ext.apply(options, {
					operations: classOperations,
					listeners: me.getBatchListeners()
				}));
			}
		}

		return me;
	}
});