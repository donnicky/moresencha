/**
 * DOM helpers.
 */
Ext.define('Mext.helpers.Dom', {
	singleton: true,

	/**
	 * Creates link tag.
	 * @param {string} href Link's destination.
	 * @param {string} innerHtml Inner HTML.
	 * @returns {string} Link.
	 */
	createLink: function (href, innerHtml) {
		var id = Ext.id();
		href = href.replace('{id}', id);
		return "<a id='" + id + "' href='" + href + "'>" + innerHtml + '</a>';
	}
});