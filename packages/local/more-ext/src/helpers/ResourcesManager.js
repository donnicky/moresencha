/**
 * @class Mext.helpers.ResourcesManager
 * @singleton
 */
Ext.define('Mext.helpers.ResourcesManager', (function () {
	var gettingPath = P.pending(),
		isGettingPath = false,
		gettingPathStart, getPathInterval, resourcesPath;

	return {
		singleton: true,

		/**
		 * @property {Integer} [ensurePathTimeout=30000] Timeout (ms) to detect resources path. Can be exceeded of application stylesheets are not loaded during specified time.
		 */
		ensurePathTimeout: 30000,

		/**
		 * @property {Integer} [pathTryInterval=300] Interval (ms) to detect if application stylesheets are loaded.
		 */
		pathTryInterval: 300,

		/**
		 * Gets the root resources path of the application. CSS resources must be loaded before this function call.
		 * @returns {String} The root resources path.
		 */
		getPath: function () {
			if (resourcesPath) return resourcesPath;
			var tempEl = Ext.getBody().createChild({cls: 'mext-res-path-inspector', style: 'display: none;'}),
				tempElStyle = window.getComputedStyle(tempEl.dom, null),
				tempElStyleBackgroundImage = tempElStyle.getPropertyValue('background-image'),
				path = (!tempElStyleBackgroundImage || tempElStyleBackgroundImage === 'none') ? null : tempElStyleBackgroundImage.match(/url\(\"*(.+)(\/more-ext\/images\/Blank\.gif)\"*\)/)[1];
			//noinspection JSAccessibilityCheck
			tempEl.destroy();
			return path;
		},

		/**
		 * Gets the root resources path of the application. If path is not available then waits for it.
		 * @returns {Promise} Promise returning resources path on success.
		 */
		ensurePath: function () {
			if (resourcesPath || isGettingPath) return gettingPath.promise;
			var me = this;
			isGettingPath = true;
			gettingPathStart = new Date().getTime();
			getPathInterval = setInterval(function () {
				resourcesPath = me.getPath();
				if (resourcesPath) {
					clearInterval(getPathInterval);
					isGettingPath = false;
					gettingPath.fulfill(resourcesPath);
				} else {
					if (new Date().getTime() - gettingPathStart > me.ensurePathTimeout) {
						clearInterval(getPathInterval);
						isGettingPath = false;
						gettingPath.reject(new Error('Timeout getting resources path.'));
					}
				}
			}, me.pathTryInterval);
			return gettingPath.promise;
		}
	};
})());