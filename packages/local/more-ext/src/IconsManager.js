Ext.define('Mext.IconsManager', {
	singleton: true,

	LOADING_INDICATOR: {
		small: 'mext-loading-indicator-small', // 16x16
		medium: 'mext-loading-indicator-medium', // 32x32
		large: 'mext-loading-indicator-large', // 48x48
		huge: 'mext-loading-indicator-huge' // 64x64
	}
});