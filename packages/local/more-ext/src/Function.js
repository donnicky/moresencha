/**
 * Function helpers
 */
Ext.define('Mext.Function', {

	singleton: true,
	/**
	 * Adds behavior to an existing method that is executed after the
	 * original behavior of the function.  For example:
	 *
	 *     var soup = {
     *         contents: [],
     *         add: function(ingredient) {
     *             this.contents.push(ingredient);
     *         }
     *     };
	 *     Ext.Function.interceptAfter(soup, "add", function(ingredient){
     *         // Always add a bit of extra salt
     *         this.contents.push("salt");
     *     });
	 *     soup.add("water");
	 *     soup.add("onions");
	 *     soup.contents; // will contain: water, salt, onions, salt
	 *
	 * @param {Object} obj The target object
	 * @param {String} methodName Name of the method to override
	 * @param {Function} fn Function with the new behavior.  It will
	 * be called with return value of original method plus the same arguments as the original method.
	 * The return value of this function will be the return value of the new method.
	 * @param {Object} [scope] The scope to execute the interceptor function. Defaults to the object.
	 * @return {Function} The new function just created.
	 */
	interceptAfter: function (obj, methodName, fn, scope) {
		var method = obj[methodName] || Ext.emptyFn;

		return (obj[methodName] = function () {
			var originalReturn = method.apply(this, arguments);
			var args = [originalReturn].concat(Array.prototype.slice.call(arguments, 0));
			return fn.apply(scope || this, args);
		});
	},

	/**
	 * Simple store load blocker that can be set as `beforeload` event handler.
	 * @returns {boolean} Returns false.
	 */
	storeLoadBlocker: function () {
		return false;
	}
});