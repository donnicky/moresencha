/**
 * Regular expressions collection.
 */
Ext.define('Mext.RegEx', {
	mixins: [
		'Mext.localization.Localizable'
	],
	requires: [
		'Mext.core.locale.Default'
	],
	singleton: true,

	/**
	 * Gets e-mail regular expression.
	 * @returns {RegExp} E-mail regular expression.
	 */
	getEmailExp: function () {
		return /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$/;
	},

	/**
	 * Gets invalid e-mail text.
	 * @returns {String} Invalid e-mail text.
	 */
	getEmailError: function () {
		return this.L('emailError');
	},

	/**
	 * Get phone regular expression.
	 * @returns {RegExp} Phone regular expression.
	 */
	getPhoneExp: function() {
		return /^\((\d{3})\)\s*(\d{3}(?:-|\s*)\d{4})$/;
	},

	/**
	 * Gets invalid phone text.
	 * @returns {String} Invalid phone text.
	 */
	getPhoneError: function () {
		return this.L('phoneError');
	}
});