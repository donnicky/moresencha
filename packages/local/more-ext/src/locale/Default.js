Ext.define('Mext.core.locale.Default', {
	requires: [
		'Mext.helpers.ResourcesManager',
		'Mext.localization.Localization'
	],
	singleton: true,
	data: {
		"root": {
			"Mext.direct.Manager": {
				"remoteApiError": "Remote API error",
				"unknownError": "Unknown error.",
				"metadataLoadingFailed": "Error loading Direct API metadata."
			},
			"Mext.RegEx": {
				"emailError": "Invalid email",
				"phoneError": "Invalid phone. Required format is (000) 000-0000"
			}
		}
	}
}, function () {
	Globalize.loadMessages(this.data);
	Mext.localization.Localization.addLocaleData(Mext.helpers.ResourcesManager.ensurePath().then(function (p) {
		return p + '/more-ext/locales/{1}-core.json'
	}));
});