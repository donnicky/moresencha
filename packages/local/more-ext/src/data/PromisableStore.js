/**
 * Makes a store promisable.
 */
Ext.define('Mext.data.PromisableStore', {
	extend: 'Ext.Mixin',
	mixinConfig: {
		id: 'mext-promisablestore',
		on: {
			constructor: 'mextPromisableStoreConstructor'
		}
	},
	requires: [
		'Mext.Function'
	],

	/**
	 * Gets loading promise. Returns promise in pending state for new stores.
	 * @returns {Promise} A promise returning ab `object` with `records, operation` properties on success or `operation.getError()` on fail.
	 */
	getLoading: function () {
		if (this.mextPromisable.operations.length === 0) {
			if (!this.mextPromisable.storeHadLoadTry)
				this.mextPromisable.operations.push({operation: null, resolver: Promise.pending()});
			else return new Promise(function (resolve) {
				resolve();
			});
		}

		return this.mextPromisable.operations[this.mextPromisable.operations.length - 1].resolver.promise;
	},

	/**
	 * Starts store loading if it has never been loaded.
	 * @returns {Promise} See {@link #getLoading} return.
	 */
	loadOnce: function () {
		if (!this.mextPromisable.storeHadLoadTry)
			this.load();
		return this.getLoading();
	},

	/**
	 * Gets loading promise aggregating all current loadings. Returns promise in pending state for new stores.
	 * @returns {Promise} A promise returning an `array` of `object` with `records, operation` properties on success or `operation.getError()` on fail.
	 */
	getAllLoadings: function () {
		if (this.mextPromisable.operations.length === 0 && !this.mextPromisable.storeHadLoadTry)
			this.mextPromisable.operations.push({operation: null, resolver: Promise.pending()});

		return Promise.all(this.mextPromisable.operations.map('resolver').map('promise'));
	},

	/**
	 * @private
	 */
	mextPromisableStoreConstructor: function () {
		var me = this;
		me.mextPromisable = {};
		me.mextPromisable.storeHadLoadTry = false;
		me.mextPromisable.operations = [];

		Mext.Function.interceptAfter(this, 'createOperation', function (operation, type) {
			if (type !== 'read') return;
			Mext.Function.interceptAfter(operation, 'execute', function (request) {
				me.mextPromisableOnReadOperaionStarted(operation);
				return request;
			});
			return operation;
		});

		me.on('load', this.mextPromisableStoreOnLoad, me);
	},

	/**
	 * @private
	 */
	mextPromisableOnReadOperaionStarted: function (operation) {
		if (this.mextPromisable.operations.length > 0 && !this.mextPromisable.storeHadLoadTry)
			this.mextPromisable.operations[0].operation = operation;
		else
			this.mextPromisable.operations.push({operation: operation, resolver: Promise.pending()});
		this.mextPromisable.storeHadLoadTry = true;
	},

	/**
	 * @private
	 */
	mextPromisableStoreOnLoad: function (store, records, successful, operation) {
		var op = this.mextPromisable.operations.find({operation: operation}),
			resolver = op.resolver;
		this.mextPromisable.operations.remove(op);
		if (operation.wasSuccessful())
			resolver.resolve({records: records, operation: operation});
		else
			resolver.reject(operation.getError());
	}
});