(function () {
	var apiLoadingOperation = Promise.pending(),
		isApiLoading = false,
		isApiLoaded = false;

	//noinspection JSUnusedGlobalSymbols
	/**
	 * @class Mext.direct.Manager
	 * @singleton
	 */
	Ext.define('Mext.direct.Manager', {
		requires: [
			'Ext.Loader',
			'Ext.direct.*',
			'Mext.core.locale.Default'
		],
		mixins: [
			'Mext.localization.Localizable'
		],
		singleton: true,

		/**
		 * Initialize Ext.Direct remote API with predefined paramters.
		 * Calls {@link #initRemoteApi}('/rpc', 'Ext.app.REMOTING_API', 120000).
		 * @returns {Promise}
		 */
		initDefaultRemoteApi: function () {
			return this.initRemoteApi('/rpc', 'Ext.app.REMOTING_API', 120000);
		},

		/**
		 * Initializes (loads API metadata and adds provider) Ext.Direct remote API.
		 * @param {String} apiUrl Remote API url to get API metadata.
		 * @param {String} remoteApi Remote API metadata variable. For example, `Ext.app.REMOTING_API`.
		 * @param {Integer} [timeout] Timeout for Direct API requests. Defaults to `30000`.
		 * @returns {Promise}
		 */
		initRemoteApi: function (apiUrl, remoteApi, timeout) {
			if (isApiLoaded || isApiLoading) return apiLoadingOperation.promise;
			var L = this.L.bind(this);
			if (!Ext.isNumber(timeout)) timeout = 30000;
			isApiLoading = true;
			//noinspection JSUnusedGlobalSymbols
			Ext.Loader.loadScript({
				url: apiUrl,
				onLoad: function () {
					eval(remoteApi + '.timeout = ' + timeout);
					eval('Ext.direct.Manager.addProvider(' + remoteApi + ');');
					//noinspection RequiresInspection
					Ext.direct.Manager.on('exception', function (event) {
						Ext.Msg.show({
							title: L('remoteApiError'),
							message: event.message || L('unknownError'),
							buttons: Ext.Msg.OK,
							icon: Ext.Msg.ERROR
						});
					});
					isApiLoading = false;
					isApiLoaded = true;
					apiLoadingOperation.fulfill();
				},
				onError: function () {
					isApiLoading = false;
					apiLoadingOperation.reject(new Error(L('metadataLoadingFailed')));
				}
			});

			return apiLoadingOperation.promise;
		},

		/**
		 * Returns {@link Promise} for remote API metadata loading operation.
		 * @returns {Promise}
		 */
		getApiLoadingOperation: function () {
			return apiLoadingOperation.promise;
		}
	});
})();