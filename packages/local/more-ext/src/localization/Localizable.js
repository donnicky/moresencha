/**
 * @class Mext.localization.Localizable
 * This class is intended as a mixin for classes that want to provide localization.
 */
Ext.define('Mext.localization.Localizable', {
	extend: 'Ext.Mixin',
	mixinConfig: {
		id: 'mext-localizable'
	},
	config: {
		/**
		 * @cfg {Globalize} [localizer] Localizer for class instance. If not set then default localizer is used.
		 */
		localizer: null
	},

	inheritableStatics: {
		/**
		 * Returns localized value for some predefined localization node of this class.
		 * @param {String} localizationNode Localization node key. For example, `greetings/hello`
		 * @returns {String} Localized value.
		 */
		L: function (localizationNode) {
			var args = Array.prototype.slice.call(arguments, 1);
			return this.LL.apply(this, [void 0, localizationNode].concat(args));
		},

		/**
		 * Returns localized value for some predefined localization node of this class.
		 * @param {Globalize} localizer Globalize instance. If not set then default localizer is used.
		 * @param {String} localizationNode Localization node key. For example, `greetings/hello`
		 * @returns {String} Localized value.
		 */
		LL: function (localizer, localizationNode) {
			var scope = localizer || Globalize,
				args = Array.prototype.slice.call(arguments, 2);

			return scope.formatMessage.apply(scope, ['{1}/{2}'.assign(this.$className, localizationNode)].concat(args));
		}
	},

	/**
	 * Returns localized value for some predefined localization node of this class.
	 * @param {String} localizationNode Localization node key. For example, `greetings/hello`
	 * @returns {String} Localized value.
	 */
	L: function (localizationNode) {
		var args = Array.prototype.slice.call(arguments, 1);
		return this.self.LL.apply(this.self, [this.getLocalizer(), localizationNode].concat(args));
	}
});