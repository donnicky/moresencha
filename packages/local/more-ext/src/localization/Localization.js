(function () {
	var localeCookieName = 'locale',
		initOptions = {
			localeInitializers: []
		},
		detectLocaleQS = 'locale',
		userDefinedCldrDataRoot,
		loadedLocaleData = {root: true},
		loadedCldrData = {root: true, en: true},
		additionalLocalData = [],
		customExtJsLocales = {
			'en-150': {
				pathInResources: '/more-ext/locales/ext-locale-en_150.js',
				base: 'en'
			},
			'nb': {
				pathInResources: '/more-ext/locales/ext-locale-nb.js',
				base: 'no_NB'
			}
		},
		availableLocales = [];

	//noinspection MismatchedClassNameInspection
	/**
	 * @class Mext.localization.Localization
	 * @singleton
	 */
	Ext.define('Mext.localization.Localization', {
		requires: [
			'Ext.Ajax',
			'Ext.JSON',
			'Ext.Loader',
			'Mext.helpers.ResourcesManager',
			'Mext.localization.cldr.*'
		],
		singleton: true,

		localeDependentRequiredCldrData: [
			'main/{1}/numbers.json',
			'main/{1}/currencies.json',
			'main/{1}/ca-gregorian.json',
			'main/{1}/timeZoneNames.json',
			'main/{1}/dateFields.json',
			'main/{1}/languages.json'
		],

		/**
		 * Sets options for localization engine.
		 * @param {Object} [options] Options for localizations engine.
		 * @param {String} [options.locale] Default locale.
		 * @param {String} [options.localeCookieName] Cookie name for default locale value. Defaults to `locale`.
		 * @param {String} [options.detectLocaleQS] URL query string param for default locale value. Defaults to `locale`.
		 * @param {String} [options.cldrDataRoot] URL to CLDR data.
		 * @param {Array<String>} [options.availableLocales] Array of available locales. The first will be fallback target. Defaults tp `[]` what means any locale.
		 * @returns {Promise}
		 */
		setOptions: function (options) {
			var promises = [];
			options = options || {};
			localeCookieName = options.localeCookieName || localeCookieName;
			detectLocaleQS = options.detectLocaleQS || detectLocaleQS;
			userDefinedCldrDataRoot = options.cldrDataRoot || userDefinedCldrDataRoot;
			availableLocales = options.availableLocales || availableLocales;
			if (options.locale)
				promises.push(this.setDefaultLocale(options.locale));
			return P.all(promises);
		},

		/**
		 * Creates localizer for specific locale.
		 * @param {String} locale Locale for created localizer.
		 * @returns {Promise} Promise returning `Globalize` instance.
		 */
		createLocalizer: function (locale) {
			return this.prepareLocalizedData(locale).then(function () {
				var localizer = Globalize(locale);
				localizer.cldr.attributes.bundle = localizer.cldr.attributes.bundle || 'root';
				return localizer;
			});
		},

		/**
		 * Sets default locale and loads corresponding localization files.
		 * @param {String} [locale] Default locale.
		 * @returns {Promise} Promise returning locale details.
		 */
		setDefaultLocale: function (locale) {
			var me = this,
				matchingLocale;

			if (!locale) {
				var qsLocale = Object.fromQueryString(window.location.search)[detectLocaleQS];
				if (qsLocale) {
					locale = qsLocale;
				} else {
					var cookieLocale = document.cookie.match(new RegExp(localeCookieName + '=([\\w-]+)'));
					cookieLocale = cookieLocale && cookieLocale[1];
					if (cookieLocale)
						locale = cookieLocale;
					else
						locale = me.getSystemLocale();
				}
			}

			if (Ext.isArray(availableLocales) && availableLocales.length > 0) {
				if (availableLocales.indexOf(locale) < 0) { // if no among available locales
					if (locale.indexOf('-') < 0) { // if no territory
						matchingLocale = availableLocales.find(new RegExp('^' + locale + '\-')); // get the same language with another territory
						if (!matchingLocale)
							locale = availableLocales[0];
						else
							locale = matchingLocale;
					}
					else {
						locale = locale.substr(0, locale.indexOf('-')); // extract language
						if (availableLocales.indexOf(locale) < 0) {
							matchingLocale = availableLocales.find(new RegExp('^' + locale + '\-')); // get the same language with another territory
							if (!matchingLocale)
								locale = availableLocales[0];
							else
								locale = matchingLocale;

						}
					}
				}
			}

			return me.prepareLocalizedData(locale).then(function () {
				Globalize.locale(locale);
				Globalize.cldr.attributes.bundle = Globalize.cldr.attributes.bundle || 'root';
				document.cookie = localeCookieName + '=' + locale + '; expires=' + new Date(new Date().getFullYear() + 50, 1, 1).toUTCString() + '; path=/';
			}).then(function () {
				//noinspection JSUndefinedPropertyAssignment
				initOptions.locale = Mext.localization.Localization.getDefaultLocale();
			}).then(function () {
				return Promise.all(initOptions.localeInitializers.map(function (localeInitializer) {
					return localeInitializer(initOptions);
				}));
			}).then(function () {
				return me.getDefaultLocaleDetails();
			});
		},

		/**
		 * Return a function that formats a number according to the given options.
		 * @param {Object} [options] A JSON object including none or any of the following options.
		 * @param {String} [options.style] `decimal` (default) or `percent`.
		 * @param {Integer} [options.minimumIntegerDigits]
		 * Non-negative integer value indicating the minimum integer digits to be used.
		 * Numbers will be padded with leading zeroes if necessary.
		 * @param {Integer} [options.minimumFractionDigits]
		 * Non-negative integer value indicating the minimum fraction digits to be used. Numbers will be rounded or padded with trailing zeroes if necessary.
		 * If this property is present it will override minimum fraction digits derived from the CLDR patterns.
		 * @param {Integer} [options.maximumFractionDigits]
		 * Non-negative integer value indicating the maximum fraction digits to be used. Numbers will be rounded or padded with trailing zeroes if necessary.
		 * If this property is present it will override maximum fraction digits derived from the CLDR patterns.
		 * @param {Integer} [options.minimumSignificantDigits]
		 * Positive integer value indicating the minimum fraction digits to be shown.
		 * If this property is present it overrides minimum integer and fraction digits.
		 * The formatter uses however many integer and fraction digits are required to display the specified number of significant digits.
		 * @param {Integer} [options.maximumSignificantDigits]
		 * Positive integer value indicating the maximum fraction digits to be shown.
		 * If this property is present it overrides maximum integer and fraction digits.
		 * The formatter uses however many integer and fraction digits are required to display the specified number of significant digits.
		 * @param {String} [options.round] Rounding method: `ceil`, `floor`, `round` (default), or `truncate`.
		 * @param {Boolean} [options.useGrouping] Value indicating whether a grouping separator should be used. Defaults to `true`.
		 * @returns {Function} A function that formats a number according to the given options. Invoked with one argument: the Number `value` to be formatted.
		 */
		numberFormatter: function (options) {
			return Globalize.numberFormatter(options);
		},

		/**
		 * Alias for {@link Mext.localization.Localization#numberFormatter}(options)(value).
		 * @param {Number} value Number value to format.
		 * @param {Object} [options] Options as described in {@link Mext.localization.Localization#numberFormatter}
		 * @returns {String} Formatted number.
		 */
		formatNumber: function (value, options) {
			return Globalize.formatNumber(value, options);
		},

		/**
		 * Returns default locale details.
		 * @returns {Object}
		 */
		getDefaultLocaleDetails: function () {
			return Globalize.locale().attributes;
		},

		/**
		 * Returns default locale name.
		 * @returns {String}
		 */
		getDefaultLocale: function () {
			return Globalize.locale().locale;
		},

		/**
		 * Return system locale.
		 * @returns {String}
		 */
		getSystemLocale: function () {
			return navigator.language;
		},

		/**
		 * Returns available locales set via {@link #setOptions}.
		 * @returns {Array<String>} Available locales.
		 */
		getAvailableLocales: function () {
			return availableLocales;
		},

		/**
		 * Adds locale data source.
		 * @param {String|Function|Promise} localDataDescriptor Locale data source descriptor.
		 */
		addLocaleData: function (localDataDescriptor) {
			if (additionalLocalData.indexOf(localDataDescriptor) < 0)
				additionalLocalData.push(localDataDescriptor);
		},

		/**
		 * Loads and sets Ext locale.
		 * @param {Object} options Options.
		 * @param {String} options.locale Locale.
		 * @param {String} [options.extLocalesPath] Ext locales path. Defaults to `[app resources root]/ext-locale`.
		 * @returns {Promise}
		 */
		setExtLocale: function (options) {
			var locale = options.locale,
				extLocalesPath = options.extLocalesPath;

			function ensureExtLocalesPath() {
				return new Promise(function (resolve, reject) {
					if (extLocalesPath) return resolve(extLocalesPath);
					Mext.helpers.ResourcesManager.ensurePath().then(function (resourcesRoot) {
						resolve(resourcesRoot + '/ext-locale');
					}).catch(function (err) {
						reject(err);
					})
				})
			}

			function loadExtLocale(extLocalesPath) {
				return new Promise(function (resolve) {
					var extJsLocale, customExtJsLocalePathInResources;
					if (customExtJsLocales[locale]) {
						extJsLocale = customExtJsLocales[locale].base;
						customExtJsLocalePathInResources = customExtJsLocales[locale].pathInResources;
					}
					else {
						if (locale.indexOf('-') > 0) {
							if (customExtJsLocales[locale.substr(0, locale.indexOf('-'))]) {
								extJsLocale = customExtJsLocales[locale.substr(0, locale.indexOf('-'))].base;
								customExtJsLocalePathInResources = customExtJsLocales[locale.substr(0, locale.indexOf('-'))].pathInResources;
							} else {
								extJsLocale = locale.replace('-', '_');
							}
						} else {
							extJsLocale = locale.replace('-', '_');
						}
					}
					if (!extJsLocale)
						loadCustomLocale();
					else {
						//noinspection JSUnusedGlobalSymbols
						Ext.Loader.loadScript({
							url: extLocalesPath + '/locale-' + extJsLocale + '.js',
							onLoad: function () {
								loadCustomLocale()
							},
							onError: function () {
								if (extJsLocale.indexOf('_') > 0) {
									//noinspection JSUnusedGlobalSymbols
									Ext.Loader.loadScript({
										url: extLocalesPath + '/locale-' + extJsLocale.substr(0, extJsLocale.indexOf('_')) + '.js',
										onLoad: function () {
											loadCustomLocale()
										},
										onError: function () {
											loadCustomLocale()
										}
									});
								} else {
									loadCustomLocale()
								}
							}
						});
					}

					function loadCustomLocale() {
						if (!customExtJsLocalePathInResources) {
							resolve();
							return;
						}

						Mext.helpers.ResourcesManager.ensurePath().then(function (resourcesRoot) {
							Ext.Loader.loadScript({
								url: resourcesRoot + customExtJsLocalePathInResources,
								onLoad: function () {
									resolve();
								},
								onError: function () {
									resolve();
								}
							});
						});
					}
				});
			}

			return ensureExtLocalesPath().then(loadExtLocale);
		},

		/**
		 * Initializes localization. Calls {@link #setOptions}, {@link #setDefaultLocale} and additional initializers provided via `options.localeInitializers`.
		 * @param {Object} options Localization options. As defined in {@link #setOptions} plus additional options as described below.
		 * @param {Boolean} [options.bypassExtLocale=false] Bypass Ext locale setting.
		 * @param {Array<Function>} [options.localeInitializers] Additional localization initializers. Functions getting `options` and returning {@link Promise}.
		 * @returns {Promise}
		 */
		init: function (options) {
			options = options || {};
			initOptions = Ext.clone(options);
			var me = this,
				localeInitializers = initOptions.localeInitializers || [],
				bypassExtLocale = initOptions.bypassExtLocale;

			if (!bypassExtLocale)
				localeInitializers.push(me.setExtLocale.bind(me));

			initOptions.localeInitializers = localeInitializers;

			return Mext.localization.Localization.setOptions(initOptions)
				.then(function () {
					if (!initOptions.locale)
						return Mext.localization.Localization.setDefaultLocale();
					return null;
				});
		},

		/**
		 * Returns current locale display name.
		 * @returns {String}
		 */
		getCurrentLocaleDisplayName: function () {
			var cldr = Globalize.locale();
			return cldr.main('localeDisplayNames/languages/' + cldr.locale)
				|| cldr.main('localeDisplayNames/languages/' + cldr.attributes.bundle)
				|| cldr.main('localeDisplayNames/languages/' + cldr.attributes.language)
				|| '---';
		},

		/**
		 * Returns flag css style for default locale.
		 * @returns {String}
		 */
		getCurrentLocaleFlagCssStyle: function () {
			return this.getLocaleFlagCssStyle(Globalize.locale());
		},

		/**
		 * Returns flag css style corresponding to the territory specified in Cldr instance.
		 * @param {Cldr|String} cldr Cldr instance or territory code.
		 * @returns {String}
		 */
		getLocaleFlagCssStyle: function (cldr) {
			var territory = Ext.isString(cldr) ? cldr : cldr.attributes.territory;
			if (!territory) return '';
			return 'mext-flag-icon-{1}-small'.assign(territory.toLowerCase());
		},

		/**
		 * Gets locales display names in their locales.
		 * @param {Array<String>} locales Locales.
		 * @returns {Promise} Promise returning array of objects: `[{locale: 'xx', territory: 'yy', displayName: 'zz'}, ...]`.
		 */
		getLocalesInfo: function (locales) {
			var me = this;
			return me.getCldrDataRoot().then(function (clrdDataRoot) {
				var tasks = [];

				locales.forEach(function (locale) {
					if (!loadedLocaleData[locale] && !loadedCldrData[locale]) {
						tasks.push(new P(function (resolve, reject) {
							Ext.Ajax.request({
								url: clrdDataRoot + '/main/{1}/languages.json'.assign(locale),
								callback: function (options, success, response) {
									if (!success) return reject(new Error('Could not load CLDR data for locale {1}'.assign(locale)));
									try {
										Globalize.load(Ext.JSON.decode(response.responseText));
										resolve();
									}
									catch (err) {
										reject(err);
									}
								}
							});
						}));
					}
				});

				return Promise.all(tasks).then(function () {
					var result = [];
					locales.forEach(function (locale) {
						var cldr = new Cldr(locale);
						result.push({locale: locale, territory: cldr.attributes.territory, displayName: new Cldr(locale).main('localeDisplayNames/languages/' + locale)});
					});
					return result;
				})
			});
		},

		/**
		 * @private
		 * Gets CLDR data root url.
		 * @returns {String}
		 */
		getCldrDataRoot: function () {
			return new P(function (resolve, reject) {
				if (userDefinedCldrDataRoot) return resolve(userDefinedCldrDataRoot);
				Mext.helpers.ResourcesManager.ensurePath().then(function (resourcesPath) {
					resolve(resourcesPath + '/cldr-data');
				}).catch(function (err) {
					reject(err);
				});
			});
		},

		/**
		 * @private
		 * Prepares localized data for given locale.
		 * @param {String} locale Locale to prepare data for.
		 * @returns {Promise}
		 */
		prepareLocalizedData: function (locale) {
			var me = this,
				language = locale.split('-')[0];

			return me.getCldrDataRoot().then(function (cldrDataRoot) {
				var loadTasks = [me.loadLocaleData(cldrDataRoot, language)];
				if (language !== locale)
					loadTasks.push(me.loadLocaleData(cldrDataRoot, locale));
				return P.all(loadTasks);
			});
		},

		/**
		 * @private
		 * Loads locale data from CLDR and custom data sources.
		 * @param cldrDataRoot CLDR data root.
		 * @param locale Locale.
		 * @returns {Promise}
		 */
		loadLocaleData: function (cldrDataRoot, locale) {
			var me = this,
				tasks = [];

			if (loadedLocaleData[locale]) {
				return new P(function (r) {
					r();
				});
			}

			return P.all(additionalLocalData.map(function (x) {
				if (Object.isFunction(x))
					return x.call(window, locale);
				if (Object.isObject(x) && Object.isFunction(x.fn))
					return x.fn.call(x.scope || swindow, locale);
				return x;
			})).then(function (localeData) {
				me.localeDependentRequiredCldrData.map(function (dataUrl) {
					return {cldr: true, url: loadedCldrData[locale] ? null : (cldrDataRoot + '/' + dataUrl)};
				}).union(localeData.map(function (x) {
					return {cldr: false, url: x};
				})).forEach(function (target) {
					var url = target.url;

					if (!url) return;

					if (Object.isObject(url)) {
						tasks.push({cldr: target.cldr, data: url});
						return;
					}

					url = url.assign(locale);
					tasks.push(new P(function (resolve, reject) {
						Ext.Ajax.request({
							url: url,
							callback: function (options, success, response) {
								if (success) {
									try {
										resolve({cldr: target.cldr, data: Ext.JSON.decode(response.responseText, false)});
									}
									catch (err) {
										reject(err);
									}
									return;
								}

								return resolve(null);
							}
						});
					}));
				});

				return P.all(tasks).then(function (loadedDataSet) {
					//<debug>
					console.debug('Loaded locale data ({1}):'.assign(locale));
					console.debug(loadedDataSet);
					//</debug>
					loadedDataSet.forEach(function (loadedData) {
						if (!loadedData) return;
						Globalize[loadedData.cldr ? 'load' : 'loadMessages'](loadedData.data);
					});
					loadedLocaleData[locale] = true;
				});
			});
		}
	}, function () {
		Globalize.load(
			Mext.localization.cldr.supplemental.LikelySubtags.data,
			Mext.localization.cldr.supplemental.NumberingSystems.data,
			Mext.localization.cldr.supplemental.Ordinals.data,
			Mext.localization.cldr.supplemental.Plurals.data,
			Mext.localization.cldr.supplemental.CurrencyData.data,
			Mext.localization.cldr.supplemental.TimeData.data,
			Mext.localization.cldr.supplemental.WeekData.data,
			Mext.localization.cldr.main.root.Numbers.data,
			Mext.localization.cldr.main.root.Currencies.data,
			Mext.localization.cldr.main.root.CaGregorian.data,
			Mext.localization.cldr.main.root.TimeZoneNames.data,
			Mext.localization.cldr.main.root.DateFields.data,
			Mext.localization.cldr.main.root.Languages.data,
			Mext.localization.cldr.main.en.Numbers.data,
			Mext.localization.cldr.main.en.Currencies.data,
			Mext.localization.cldr.main.en.CaGregorian.data,
			Mext.localization.cldr.main.en.TimeZoneNames.data,
			Mext.localization.cldr.main.en.DateFields.data,
			Mext.localization.cldr.main.en.Languages.data);
		Globalize.locale('en');
	});
})();