cd packages/local
cd bluebird
sencha package build
cd ..
cd jquery
sencha package build
cd ..
cd lodash
sencha package build
cd ..
cd moment
sencha package build
cd ..
cd more-ext
sencha package build
cd ..
cd sugarjs
sencha package build
cd ..
cd jsnlog
sencha package build
cd ..
cd lz-string
sencha package build
cd ..
cd ..
cd ..