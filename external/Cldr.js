/**
 * @class Cldr
 * [CLDR (unicode.org)](http://cldr.unicode.org/) provides locale content for i18n software. The data is provided in two formats: LDML (XML format) and JSON.
 * [Cldr.js](https://github.com/rxaviers/cldrjs) goal is to provide a simple layer to facilitate i18n software to access and use the [official CLDR JSON data](http://cldr.unicode.org/index/cldr-spec/json).
 */