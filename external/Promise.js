/**
 * @class Promise
 *
 * [Bluebird](https://github.com/petkaantonov/bluebird/blob/master/API.md) implementation of Promise pattern.
 */