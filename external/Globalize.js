/**
 * @class Globalize
 *
 * A [JavaScript library](https://github.com/jquery/globalize) for internationalization and localization that leverages the official Unicode CLDR JSON data.
 */